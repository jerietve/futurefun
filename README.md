FutureFun
=========

FutureFun is the code name of my first computer graphics project, which I wrote for a class I took in university. It is described in more detail on my [blog](https://friendsofj.wordpress.com/2013/02/28/futurefun-my-first-computer-graphics-project/ "Friends of J")

Dependencies
------------
In order to be able to compile and run this project, you will need to link the jogl-all.jar library and the gluegen-rt.jar library from the [jogamp](http://jogamp.org/deployment/jogamp-current/archive/) website. At least when adding these in NetBeans, the native jars should automatically be selected.
You will also need an OpenGL 4.1 capable video card. If your video card magically only supports OpenGL 4.0 but no 4.1, you can change all the first lines in the .glsl files in the shaders folder from '410' to '400'.

User Features
-------------
As a player you can do the following things:

In both free and third person mode:

* __1__: Make the cat wave its hands
* __2__: Make the cat walk forward
* __,__: Make the cat turn left
* __.__: Make the cat turn right
* __4__: Show bounding boxes
* __T__: Show polygons
* __CTRL__: Slow down movement of the camera or third-person movement
* __SHIFT__: Speed up movement of the camera or third-person movement
* __C__: Switch between free and third person mode
* __ESC__: Exit game

Extra in third person mode:

* __W, A, D__: Move the cat

Extra in free mode:

* __W, A, S, D, F1, F4, arrow keys__: Move the camera