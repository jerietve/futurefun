package exceptions;


/**
 * Exception to specify that a library element was uninitialized at the time a dependent funtion was called.
 *
 * @author Maarten van Meersbergen
 */
public class UninitializedException extends Exception
{
	private static final long serialVersionUID = 7346571330247360360L;
}
