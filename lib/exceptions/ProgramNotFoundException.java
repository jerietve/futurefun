package exceptions;


/**
 * Exception to specify that a shader program was not found.
 *
 * @author Maarten van Meersbergen
 */
public class ProgramNotFoundException extends Exception
{
	private static final long serialVersionUID = -402272612566630421L;
}
