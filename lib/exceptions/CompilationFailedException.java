package exceptions;


/**
 * Exception to specify that a shader compilation was unsuccessful.
 *
 * @author Maarten van Meersbergen
 */
public class CompilationFailedException extends Exception
{
	private static final long serialVersionUID = 5998388867348030689L;


	public CompilationFailedException(String e)
	{
		super(e);
	}
}
