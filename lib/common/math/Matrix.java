package common.math;

import java.nio.FloatBuffer;


/**
 * An abstract class that represents any Matrix.
 *
 * @author Maarten van Meersbergen
 */
public abstract class Matrix
{
	protected float m[];
	protected FloatBuffer buf;


	protected Matrix(int size)
	{
		this.m = new float[size];
		this.buf = FloatBuffer.wrap(this.m);
		this.buf.rewind();
	}


	/**
	 * Returns the flattened Array associated with this matrix.
	 *
	 * @return This matrix as a flat Array.
	 */
	public float[] asArray()
	{
		return this.m;
	}


	/**
	 * Returns the FloatBuffer associated with this matrix.
	 *
	 * @return This matrix as a FloatBuffer.
	 */
	public FloatBuffer asBuffer()
	{
		this.buf.rewind();
		return this.buf;
	}


	/**
	 * Returns the value of this matrix at position i.
	 *
	 * @param i The index.
	 *
	 * @return The value at index i.
	 */
	public float get(int i)
	{
		return this.m[i];
	}


	/**
	 * Returns the value of this matrix at position i,j.
	 *
	 * @param i The row.
	 * @param j The column.
	 *
	 * @return The value at index i,j.
	 */
	public float get(int i, int j)
	{
		final int rowSize = (int) Math.sqrt(this.m.length);
		return this.m[i * rowSize + j];
	}


	/**
	 * Sets the value of this matrix at position i.
	 *
	 * @param i The position (indexed row first).
	 * @param f The new value.
	 */
	public void set(int i, float f)
	{
		this.m[i] = f;
	}


	/**
	 * Sets the value of this matrix at position i,j.
	 *
	 * @param i The row.
	 * @param j The column.
	 * @param f The new value.
	 */
	public void set(int i, int j, float f)
	{
		final int rowSize = (int) Math.sqrt(this.m.length);
		this.m[i * rowSize + j] = f;
	}


	@Override
	public String toString()
	{
		final int rowSize = (int) Math.sqrt(this.m.length);
		String result = "";

		for(int i = 0; i < this.m.length; i++)
		{
			if((i != 0) && (i % rowSize == 0))
			{
				result += "\n";
			}

			result += this.m[i] + " ";
		}

		return result;
	}
}
