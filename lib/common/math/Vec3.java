package common.math;


/**
 * A class that represents a 3-place vector.
 *
 * @author Maarten van Meersbergen
 */
public class Vec3 extends Vector
{
	public static Vec3 X_AXIS_N = new Vec3(1, 0, 0);
	public static Vec3 Y_AXIS_N = new Vec3(0, 1, 0);
	public static Vec3 Z_AXIS_N = new Vec3(0, 0, 1);


	/**
	 * Creates a new vector, initialized to 0.
	 */
	public Vec3()
	{
		super(3);
		this.v[0] = 0f;
		this.v[1] = 0f;
		this.v[2] = 0f;
	}


	/**
	 * Creates a new vector with the given values.
	 *
	 * @param x The value to be put in the first position.
	 * @param y The value to be put in the second position.
	 * @param z The value to be put in the third position.
	 */
	public Vec3(float x, float y, float z)
	{
		super(3);
		this.v[0] = x;
		this.v[1] = y;
		this.v[2] = z;
	}


	/**
	 * Creates a new vector by copying the given vector.
	 *
	 * @param v The vector to be copied.
	 */
	public Vec3(Vec3 v)
	{
		super(3);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
		this.v[2] = v.v[2];
	}


	/**
	 * Creates a new vector by copying the given vector ans stripping the final position off.
	 *
	 * @param v The vector to be copied.
	 */
	public Vec3(Vec4 v)
	{
		super(3);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
		this.v[2] = v.v[2];
	}


	/**
	 * Adds the given vector to the current vector, and returns the result.
	 *
	 * @param u The vector to be added to this vector.
	 *
	 * @return The new vector.
	 */
	public Vec3 add(Vec3 u)
	{
		final Vec3 result = new Vec3();
		result.v[0] = this.v[0] + u.v[0];
		result.v[1] = this.v[1] + u.v[1];
		result.v[2] = this.v[2] + u.v[2];
		return result;
	}


	@Override
	public Vec3 clone()
	{
		return new Vec3(this);
	}


	/**
	 * Divides the current vector with the given scalar.
	 *
	 * @param n The scalar to be divided with.
	 *
	 * @return The new Vector, which is a result of the division.
	 */
	public Vec3 div(Number n)
	{
		final float f = n.floatValue();
		if(f == 0f)
		{
			return new Vec3();
		}
		final float fn = 1f / f;

		final Vec3 result = new Vec3();
		result.v[0] = this.v[0] * fn;
		result.v[1] = this.v[1] * fn;
		result.v[2] = this.v[2] * fn;
		return result;
	}


	@Override
	public boolean equals(Object thatObject)
	{
		if(this == thatObject)
		{
			return true;
		}
		if(!(thatObject instanceof Vec3))
		{
			return false;
		}

		// cast to native object is now safe
		final Vec3 that = (Vec3) thatObject;

		// now a proper field-by-field evaluation can be made
		return ((this.v[0] == that.v[0]) && (this.v[1] == that.v[1]) && (this.v[2] == that.v[2]));
	}


	@Override
	public int hashCode()
	{
		final int hashCode = (int) (this.v[0] + 23 * 6833 + this.v[1] + 7 * 7207 + this.v[2] + 11 * 7919);
		return hashCode;
	}


	/**
	 * Multiplies the given scalar with this vector.
	 *
	 * @param n The scalar to be multiplied with this one.
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public Vec3 mul(Number n)
	{
		final float fn = n.floatValue();
		final Vec3 result = new Vec3();
		result.v[0] = this.v[0] * fn;
		result.v[1] = this.v[1] * fn;
		result.v[2] = this.v[2] * fn;
		return result;
	}


	/**
	 * Multiplies the given vector with this vector.
	 *
	 * @param u The vector to be multiplied with this one.
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public Vec3 mul(Vec3 u)
	{
		final Vec3 result = new Vec3();
		result.v[0] = this.v[0] * u.v[0];
		result.v[1] = this.v[1] * u.v[1];
		result.v[2] = this.v[2] * u.v[2];
		return result;
	}


	/**
	 * Gives the negated vector of this vector.
	 *
	 * @return The new negated vector.
	 */
	public Vec3 neg()
	{
		final Vec3 result = new Vec3();
		result.v[0] = -this.v[0];
		result.v[1] = -this.v[1];
		result.v[2] = -this.v[2];
		return result;
	}


	/**
	 * Substracts the given vector from this vector.
	 *
	 * @param u The vector to be substracted from this one.
	 *
	 * @return The new Vector, which is a result of the substraction.
	 */
	public Vec3 sub(Vec3 u)
	{
		final Vec3 result = new Vec3();
		result.v[0] = this.v[0] - u.v[0];
		result.v[1] = this.v[1] - u.v[1];
		result.v[2] = this.v[2] - u.v[2];
		return result;
	}
}
