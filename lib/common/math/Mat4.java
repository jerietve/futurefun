package common.math;

import java.util.Arrays;
import com.jogamp.common.nio.Buffers;


/**
 * A class that represents a 4x4 Matrix.
 *
 * @author Maarten van Meersbergen
 */
public class Mat4 extends Matrix
{
	/**
	 * Creates a new 4x4 identity matrix.
	 */
	public Mat4()
	{
		super(16);
		identity();
	}


	/**
	 * Creates a new 4x4 matrix with all slots filled with the parameter.
	 *
	 * @param in The value to be put in all matrix fields.
	 */
	public Mat4(float in)
	{
		super(16);
		Arrays.fill(this.m, in);
	}


	/**
	 * Creates a new 4x4 matrix using the 16 parameters row-wise as filling.
	 *
	 * @param m00 The parameter on position 0x0.
	 * @param m01 The parameter on position 0x1.
	 * @param m02 The parameter on position 0x2.
	 * @param m03 The parameter on position 0x3.
	 * @param m10 The parameter on position 1x0.
	 * @param m11 The parameter on position 1x1.
	 * @param m12 The parameter on position 1x2.
	 * @param m13 The parameter on position 1x3.
	 * @param m20 The parameter on position 2x0.
	 * @param m21 The parameter on position 2x1.
	 * @param m22 The parameter on position 2x2.
	 * @param m23 The parameter on position 2x3.
	 * @param m30 The parameter on position 3x0.
	 * @param m31 The parameter on position 3x1.
	 * @param m32 The parameter on position 3x2.
	 * @param m33 The parameter on position 3x3.
	 */
	public Mat4(float m00, float m01, float m02, float m03, float m10, float m11, float m12, float m13, float m20,
			float m21, float m22, float m23, float m30, float m31, float m32, float m33)
	{
		super(16);
		this.m[0] = m00;
		this.m[1] = m01;
		this.m[2] = m02;
		this.m[3] = m03;
		this.m[4] = m10;
		this.m[5] = m11;
		this.m[6] = m12;
		this.m[7] = m13;
		this.m[8] = m20;
		this.m[9] = m21;
		this.m[10] = m22;
		this.m[11] = m23;
		this.m[12] = m30;
		this.m[13] = m31;
		this.m[14] = m32;
		this.m[15] = m33;
	}


	/**
	 * Creates a new 4x4 matrix by copying the matrix used as parameter.
	 *
	 * @param n The old matrix to be copied.
	 */
	public Mat4(Mat4 n)
	{
		super(16);
		this.buf.put(Buffers.copyFloatBuffer(n.asBuffer()));
	}


	/**
	 * Creates a new 4x4 matrix, using the 4 vectors in order as filling.
	 *
	 * @param v0 The first row of the matrix.
	 * @param v1 The second row of the matrix.
	 * @param v2 The third row of the matrix.
	 * @param v3 The fourth row of the matrix.
	 */
	public Mat4(Vec4 v0, Vec4 v1, Vec4 v2, Vec4 v3)
	{
		super(16);
		this.buf.put(v0.asBuffer());
		this.buf.put(v1.asBuffer());
		this.buf.put(v2.asBuffer());
		this.buf.put(v3.asBuffer());
	}


	/**
	 * Adds the given matrix to this matrix.
	 *
	 * @param n The matrix to be added to this.
	 *
	 * @return this after the addition.
	 */
	public Mat4 add(Mat4 n)
	{
		for(int i = 0; i < 16; ++i)
		{
			this.m[i] += n.m[i];
		}

		return this;
	}


	@Override
	public Mat4 clone()
	{
		return new Mat4(this);
	}


	/**
	 * Divides the elements of this matrix with the given scalar, returning this.
	 *
	 * @param n The scalar with which to divide the values of the current matrix.
	 *
	 * @return this after division.
	 */
	public Mat4 div(Number n)
	{
		final float fn = 1f / n.floatValue();

		for(int i = 0; i < 16; ++i)
		{
			this.m[i] *= fn;
		}

		return this;
	}


	private void identity()
	{
		Arrays.fill(this.m, 0f);
		this.m[0] = this.m[5] = this.m[10] = this.m[15] = 1.0f;
	}


	/**
	 * Multiplies this matrix with the given matrix, returning a new matrix. It calculates this times n, with this on
	 * the left and n on the right. It leaves this matrix unchanged.
	 *
	 * @param n The matrix to be multiplied with the current matrix.
	 *
	 * @return The new 4x4 matrix that is the result of the multiplication.
	 */
	public Mat4 mul(Mat4 n)
	{
		final Mat4 a = new Mat4(0);

		// Iterate over rows of destination matrix
		for(int i = 0; i < 4; ++i)
		{
			// Iterate over columns of destination matrix
			for(int j = 0; j < 4; ++j)
			{
				// Do four multiplications
				for(int k = 0; k < 4; ++k)
				{
					a.m[i * 4 + j] += this.m[i * 4 + k] * n.m[k * 4 + j];
				}
			}
		}

		return a;
	}


	/**
	 * Multiplies this matrix with the given scalar, returning this.
	 *
	 * @param n The scalar to be multiplied with the current matrix.
	 *
	 * @return This after multiplication.
	 */
	public Mat4 mul(Number n)
	{
		final float fn = n.floatValue();
		for(int i = 0; i < 16; ++i)
		{
			this.m[i] *= fn;
		}

		return this;
	}


	/**
	 * Multiplies this matrix with the given vector, returning a new vector.
	 *
	 * @param v The vector to be multiplied with the current matrix.
	 *
	 * @return The new 4-place vector that is the result of the multiplication.
	 */
	public Vec4 mul(Vec4 v)
	{
		return new Vec4(this.m[0 * 4 + 0] * v.v[0] + this.m[0 * 4 + 1] * v.v[1] + this.m[0 * 4 + 2] * v.v[2]
				+ this.m[0 * 4 + 3] * v.v[3], this.m[1 * 4 + 0] * v.v[0] + this.m[1 * 4 + 1] * v.v[1]
				+ this.m[1 * 4 + 2] * v.v[2] + this.m[1 * 4 + 3] * v.v[3], this.m[2 * 4 + 0] * v.v[0]
				+ this.m[2 * 4 + 1] * v.v[1] + this.m[2 * 4 + 2] * v.v[2] + this.m[2 * 4 + 3] * v.v[3],
				this.m[3 * 4 + 0] * v.v[0] + this.m[3 * 4 + 1] * v.v[1] + this.m[3 * 4 + 2] * v.v[2]
				+ this.m[3 * 4 + 3] * v.v[3]);
	}


	/**
	 * Subtracts this matrix with the given matrix, returning this.
	 *
	 * @param n The matrix to be subtracted from to the current matrix.
	 *
	 * @return this after subtraction.
	 */
	public Mat4 sub(Mat4 n)
	{
		for(int i = 0; i < 16; ++i)
		{
			this.m[i] -= n.m[i];
		}

		return this;
	}
}
