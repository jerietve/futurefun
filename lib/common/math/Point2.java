package common.math;


/**
 * Wrapper for the Vec2 class.
 *
 * @author Thilo Kielmann
 */
public class Point2 extends Vec2
{
	/**
	 * Stand-in for a 2-place vector.
	 */
	public Point2()
	{
		super();
	}


	/**
	 * Stand-in for a 2-place vector.
	 *
	 * @param x The x value of this point.
	 * @param y The y value of this point.
	 */
	public Point2(float x, float y)
	{
		super(x, y);
	}


	/**
	 * Stand-in for a 2-place vector.
	 *
	 * @param vec The xy values of this point; the 3rd place will be discarded.
	 */
	public Point2(Vec3 vec)
	{
		super(vec);
	}


	/**
	 * Stand-in for a 2-place vector.
	 *
	 * @param v A 2-place vector.
	 */
	public Point2(Vec2 vec)
	{
		super(vec);
	}


	/**
	 * Stand-in for a 2-place vector.
	 *
	 * @param v A vector of which the 3rd and 4th places will be discarded.
	 */
	public Point2(Vec4 v)
	{
		super(v);
	}
}
