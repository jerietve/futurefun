package common.math;


/**
 * A class that represents a 4-place vector.
 *
 * @author Maarten van Meersbergen
 */
public class Vec4 extends Vector
{
	public final static Vec4 X_AXIS_N = new Vec4(1, 0, 0, 0);
	public final static Vec4 Y_AXIS_N = new Vec4(0, 1, 0, 0);
	public final static Vec4 Z_AXIS_N = new Vec4(0, 0, 1, 0);
	public final static Vec4 X_AXIS_N_NEG = new Vec4(-1, 0, 0, 0);
	public final static Vec4 Y_AXIS_N_NEG = new Vec4(0, -1, 0, 0);
	public final static Vec4 Z_AXIS_N_NEG = new Vec4(0, 0, -1, 0);



	/**
	 * Creates a new vector, initialized to 0.
	 */
	public Vec4()
	{
		super(4);
		this.v[0] = 0f;
		this.v[1] = 0f;
		this.v[2] = 0f;
		this.v[3] = 0f;
	}


	/**
	 * Creates a new vector with the given values.
	 *
	 * @param x The value to be put in the first position.
	 * @param y The value to be put in the second position.
	 * @param z The value to be put in the third position.
	 * @param w The value to be put in the fourth position.
	 */
	public Vec4(float x, float y, float z, float w)
	{
		super(4);
		this.v[0] = x;
		this.v[1] = y;
		this.v[2] = z;
		this.v[3] = w;
	}


	/**
	 * Creates a new vector by copying the given vector, supplemented by the scalar.
	 *
	 * @param v The vector to be copied.
	 * @param v3 The additional value to be put into the fourth index.
	 */
	public Vec4(Vec3 v, float v3)
	{
		super(4);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
		this.v[2] = v.v[2];
		this.v[3] = v3;
	}


	/**
	 * Creates a new vector by copying the given vector.
	 *
	 * @param v The vector to be copied.
	 */
	public Vec4(Vec4 v)
	{
		super(4);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
		this.v[2] = v.v[2];
		this.v[3] = v.v[3];
	}


	/**
	 * Adds the given vector to the current vector, and returns the result.
	 *
	 * @param u The vector to be added to this vector.
	 *
	 * @return The new vector.
	 */
	public void add(Vec4 u)
	{
		v[0] += u.v[0];
		v[1] += u.v[1];
		v[2] += u.v[2];
		v[3] += u.v[3];
	}


	public Vec4 addV(Vec4 u)
	{
		add(u);
		return this;
	}


	@Override
	public Vec4 clone()
	{
		return new Vec4(this);
	}


	/**
	 * Divides the current vector with the given scalar.
	 *
	 * @param n The scalar to be divided with.
	 */
	public void div(float f)
	{
		if(f == 0f)
		{
			v[0] = v[1] = v[2] = v[3] = 0f;
		}
		final float fn = 1f / f;

		v[0] *= fn;
		v[1] *= fn;
		v[2] *= fn;
		v[3] *= fn;
	}


	public Vec4 divV(float f)
	{
		div(f);
		return this;
	}


	@Override
	public boolean equals(Object thatObject)
	{
		if(this == thatObject)
		{
			return true;
		}
		if(!(thatObject instanceof Vec4))
		{
			return false;
		}

		// cast to native object is now safe
		final Vec4 that = (Vec4) thatObject;

		// now a proper field-by-field evaluation can be made
		return ((this.v[0] == that.v[0]) && (this.v[1] == that.v[1]) && (this.v[2] == that.v[2]) && (this.v[3] == that.v[3]));
	}


	@Override
	public int hashCode()
	{
		final int hashCode = (int) (this.v[0] + 23 * 6833 + this.v[1] + 7 * 7207 + this.v[2] + 11 * 7919 + this.v[3] + 3 * 3);
		return hashCode;
	}


	/**
	 * Multiplies the given scalar with this vector.
	 *
	 * @param n The scalar to be multiplied with this one.
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public void mul(float f)
	{
		v[0] *= f;
		v[1] *= f;
		v[2] *= f;
		v[3] *= f;
	}


	public Vec4 mulV(float f)
	{
		mul(f);
		return this;
	}


	/**
	 * Multiplies the given vector with this vector.
	 *
	 * @param u The vector to be multiplied with this one.
	 */
	public void mul(Vec4 u)
	{
		v[0] *= u.v[0];
		v[1] *= u.v[1];
		v[2] *= u.v[2];
		v[3] *= u.v[3];
	}


	public Vec4 mulV(Vec4 u)
	{
		mul(u);
		return this;
	}


	/**
	 * Negates this vector.
	 */
	public void neg()
	{
		v[0] = -v[0];
		v[1] = -v[1];
		v[2] = -v[2];
		v[3] = -v[3];
	}


	public Vec4 negV()
	{
		neg();
		return this;
	}


	/**
	 * Subtracts the given vector from this vector.
	 *
	 * @param u The vector to be subtracted from this one.
	 */
	public void sub(Vec4 u)
	{
		v[0] -= u.v[0];
		v[1] -= u.v[1];
		v[2] -= u.v[2];
		v[3] -= u.v[3];
	}


	public Vec4 subV(Vec4 u)
	{
		sub(u);
		return this;
	}


	public void normalize()
	{
		div(length());
	}


	public Vec4 normalizeV()
	{
		normalize();
		return this;
	}


	public float length()
	{
		return (float) Math.sqrt(VectorMath.dot(this, this));
	}
}
