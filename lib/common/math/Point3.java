package common.math;


/**
 * Wrapper for the Vec3 class.
 *
 * @author Thilo Kielmann
 */
public class Point3 extends Vec3
{
	/**
	 * Stand-in for a 3-place vector.
	 */
	public Point3()
	{
		super();
	}


	/**
	 * Stand-in for a 3-place vector.
	 *
	 * @param x The x value of this point.
	 * @param y The y value of this point.
	 * @param z The z value of this point.
	 */
	public Point3(float x, float y, float z)
	{
		super(x, y, z);
	}


	/**
	 * Stand-in for a 3-place vector.
	 *
	 * @param vec The xyz values of this point.
	 */
	public Point3(Vec3 vec)
	{
		super(vec);
	}


	public Point3(Vec4 vec)
	{
		super(vec);
	}
}
