package common.math;


/**
 * A library for matrix manipulation.
 *
 * @author Maarten van Meersbergen
 */
public class MatrixMath
{
	/**
	 * Helper method that creates a Frustum matrix
	 *
	 * @param left The left clipping plane
	 * @param right The right clipping plane
	 * @param bottom The bottom clipping plane
	 * @param top The top clipping plane
	 * @param zNear The near clipping plane
	 * @param zFar The far clipping plane
	 *
	 * @return An frustum matrix
	 */
	public static Mat4 frustum(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		final float dX = right - left;
		final float dY = top - bottom;
		final float dZ = zFar - zNear;
		final float n = zNear;
		final float f = zFar;
		final float t = top;
		final float b = bottom;
		final float r = right;
		final float l = left;

		final Mat4 m = new Mat4(2 * n / dX, 0, (r + l) / dX, 0, 0, 2 * n / dY, (t + b) / dY, 0, 0, 0, -(f + n) / dZ, -2
				* f * n / dZ, 0, 0, -1, 0);
		return m;
	}


	/**
	 * Helper method that supplies a rotation matrix that allows us to look at the indicated point
	 *
	 * @param eye The coordinates of the eye (camera)
	 * @param at The coordinates of the object we want to look at
	 * @param up The vector indicating the up direction for the camera
	 *
	 * @return A rotation matrix suitable for multiplication with the perspective matrix
	 */
	public static Mat4 lookAt(Vec4 eye, Vec4 at, Vec4 up)
	{
		final Vec4 eyeneg = VectorMath.neg(eye);

		final Vec4 n = VectorMath.sub(eye, at).normalizeV();
		final Vec4 u = VectorMath.cross(up, n).normalizeV();
		final Vec4 v = VectorMath.cross(n, u).normalizeV();
		final Vec4 t = new Vec4(0, 0, 0, 1);
		final Mat4 c = new Mat4(u, v, n, t);

		return c.mul(MatrixMath.translate(eyeneg));
	}


	/**
	 * Helper method that supplies a rotation matrix that allows us to look at the indicated point
	 *
	 * @param x The x-coordinates of the eye (camera)
	 * @param y The y-coordinates of the eye (camera)
	 * @param z The z-coordinates of the eye (camera)
	 * @param at The coordinates of the object we want to look at
	 * @param up The vector indicating the up direction for the camera
	 *
	 * @return A rotation matrix suitable for multiplication with the perspective matrix
	 */
	public static Mat4 lookAt(float x, float y, float z, Vec4 at, Vec4 up)
	{
		final Vec4 eye = new Vec4(x, y, z, 1.0f);
		return lookAt(eye, at, up);
	}


	/**
	 * Helper method that creates a Orthogonal matrix
	 *
	 * @param left The left clipping plane
	 * @param right The right clipping plane
	 * @param bottom The bottom clipping plane
	 * @param top The top clipping plane
	 * @param zNear The near clipping plane
	 * @param zFar The far clipping plane
	 *
	 * @return An orthogonal matrix
	 */
	public static Mat4 ortho(float left, float right, float bottom, float top, float zNear, float zFar)
	{
		final float dX = right - left;
		final float dY = top - bottom;
		final float dZ = zFar - zNear;
		final float n = zNear;
		final float f = zFar;
		final float t = top;
		final float b = bottom;
		final float r = right;
		final float l = left;

		final Mat4 m = new Mat4(2.0f / dX, 0.0f, 0.0f, -((l + r) / dX),
				0.0f, 2.0f / dY, 0.0f, -((t + b) / dY),
				0.0f, 0.0f, -(2.0f / (f - n)), -((f + n) / dZ),
				0.0f, 0.0f, 0.0f, 1.0f);
		return m;
	}


	/**
	 * Helper method to define an orthogonal matrix for 2d projections
	 *
	 * @param left The left clipping plane
	 * @param right The right clipping plane
	 * @param bottom The bottom clipping plane
	 * @param top The top clipping plane
	 *
	 * @return An orthogonal matrix
	 */
	public static Mat4 ortho2D(float left, float right, float bottom, float top)
	{
		return MatrixMath.ortho(left, right, bottom, top, -1, 1);
	}


	/**
	 * Helper method that creates a perspective matrix
	 *
	 * @param fovy The fov in y-direction, in radians
	 * @param aspect The aspect ratio
	 * @param zNear The near clipping plane
	 * @param zFar The far clipping plane
	 *
	 * @return A perspective matrix, as explained on Page 268 of the book
	 */
	public static Mat4 perspective(float fovy, float aspect, float near, float far)
	{

		final float top = (float) (Math.tan(fovy / 2) * near);
		final float right = top * aspect;

		final Mat4 m = new Mat4((near / right), 0.0f, 0.0f, 0.0f,
				0.0f, (near / top), 0.0f, 0.0f,
				0.0f, 0.0f, -(far + near) / (far - near), -2.0f * far * near / (far - near),
				0.0f, 0.0f, -1.0f, 0.0f);


		return m;
	}


	/**
	 * Helper method that creates a matrix describing a rotation around an arbitrary axis
	 *
	 * @param angle The rotation angle, in radians
	 * @param x The x component of the vector that describes the axis to rotate around
	 * @param y The y component of the vector that describes the axis to rotate around
	 * @param z The z component of the vector that describes the axis to rotate around
	 *
	 * @return The rotation matrix
	 */
	public static Mat4 rotate(float angle, float x, float y, float z)
	{
		final float c = (float) Math.cos(angle);
		final float s = (float) Math.sin(angle);
		final float t = 1 - c;

		final Vec3 n = VectorMath.normalize(new Vec3(x, y, z));
		x = n.v[0];
		y = n.v[1];
		z = n.v[2];

		final Mat4 R = new Mat4(t * x * x + c, t * x * y - s * z, t * x * z + s * y, 0f, t * x * y + s * z, t * y * y
				+ c, t * y * z - s * x, 0f, t * x * z - s * y, t * y * z + s * x, t * z * z + c, 0f, 0f, 0f, 0f, 1f);

		return R;
	}


	/**
	 * Helper method that creates a matrix describing a rotation around an arbitrary axis
	 *
	 * @param angle The rotation angle, in radians
	 * @param axis The axis to rotate around
	 *
	 * @return The rotation matrix
	 */
	public static Mat4 rotate(float angle, Vec3 axis)
	{
		final Mat4 R = MatrixMath.rotate(angle, axis.get(0), axis.get(1), axis.get(2));

		return R;
	}


	/**
	 * Helper method that creates a matrix describing a rotation around an arbitrary axis
	 *
	 * @param angle The rotation angle, in radians
	 * @param axis The axis to rotate around
	 *
	 * @return The rotation matrix
	 */
	public static Mat4 rotate(float angle, Vec4 axis)
	{
		final Mat4 R = MatrixMath.rotate(angle, axis.get(0), axis.get(1), axis.get(2));

		return R;
	}


	/**
	 * Helper method that creates a matrix describing a rotation around the x-axis
	 *
	 * @param angle The rotation angle, in radians
	 *
	 * @return The rotation matrix
	 */
	public static Mat4 rotationX(float angle)
	{
		final float ca = (float) Math.cos(angle);
		final float sa = (float) Math.sin(angle);

		final Mat4 m = new Mat4(1, 0, 0, 0, 0, ca, -sa, 0, 0, sa, ca, 0, 0, 0, 0, 1);
		return m;
	}


	/**
	 * Helper method that creates a matrix describing a rotation around the y-axis
	 *
	 * @param angle The rotation angle, in radians
	 *
	 * @return The rotation matrix
	 */
	public static Mat4 rotationY(float angle)
	{
		final float ca = (float) Math.cos(angle);
		final float sa = (float) Math.sin(angle);

		final Mat4 m = new Mat4(ca, 0, sa, 0, 0, 1, 0, 0, -sa, 0, ca, 0, 0, 0, 0, 1);

		return m;
	}


	/**
	 * Helper method that creates a matrix describing a rotation around the z-axis
	 *
	 * @param angle The rotation angle, in radians
	 *
	 * @return The rotation matrix
	 */
	public static Mat4 rotationZ(float angle)
	{
		final float ca = (float) Math.cos(angle);
		final float sa = (float) Math.sin(angle);

		final Mat4 m = new Mat4(ca, -sa, 0, 0, sa, ca, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);

		return m;
	}


	/**
	 * Helper method that creates a scaling matrix
	 *
	 * @param x The x scale
	 * @param y The y scale
	 * @param z The z scale
	 *
	 * @return A translation matrix
	 */
	public static Mat4 scale(float x, float y, float z)
	{
		final Mat4 m = new Mat4(x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1);
		return m;
	}


	/**
	 * Helper method that creates a shearing matrix
	 *
	 * @param x The factor that the x-dimension is sheared with
	 *
	 * @return A transformation matrix
	 */
	public static Mat4 shearX(float x)
	{
		//you need to insert the x value
		//into the right position in the matrix below
		return new Mat4(1.0f, 1 / (float) Math.cos(x), 0.0f, 0.0f,
				0.0f, 1.0f, 0.0f, 0.0f,
				0.0f, 0.0f, 1.0f, 0.0f,
				0.0f, 0.0f, 0.0f, 1.0f);
	}


	/**
	 * Helper method that creates a translation matrix
	 *
	 * @param x The x translation
	 * @param y The y translation
	 * @param z The z translation
	 *
	 * @return A translation matrix
	 */
	public static Mat4 translate(float x, float y, float z)
	{
		final Mat4 m = new Mat4(1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1);
		return m;
	}


	/**
	 * Helper method that creates a translation matrix
	 *
	 * @param vec The vector with which we want to translate
	 *
	 * @return A translation matrix
	 */
	public static Mat4 translate(Vec3 vec)
	{
		return MatrixMath.translate(vec.v[0], vec.v[1], vec.v[2]);
	}


	/**
	 * Helper method that creates a translation matrix
	 *
	 * @param vec The vector with which we want to translate
	 *
	 * @return A translation matrix
	 */
	public static Mat4 translate(Vec4 vec)
	{
		return MatrixMath.translate(vec.v[0], vec.v[1], vec.v[2]);
	}


	/**
	 * Helper method that creates a scaling matrix
	 *
	 * @param vec The vector with which we want to scale
	 *
	 * @return A translation matrix
	 */
	public static Mat4 Scale(Vec3 vec)
	{
		return MatrixMath.scale(vec.v[0], vec.v[1], vec.v[2]);
	}


	/**
	 * Helper method that creates a scaling matrix
	 *
	 * @param vec The vector with which we want to scale
	 *
	 * @return A translation matrix
	 */
	public static Mat4 Scale(Vec4 vec)
	{
		return MatrixMath.scale(vec.v[0], vec.v[1], vec.v[2]);
	}


	public static Mat4 transpose(Mat4 m)
	{
		return new Mat4(m.get(0, 0), m.get(1, 0), m.get(2, 0), m.get(3, 0),
				m.get(0, 1), m.get(1, 1), m.get(2, 1), m.get(3, 1),
				m.get(0, 2), m.get(1, 2), m.get(2, 2), m.get(3, 2),
				m.get(0, 3), m.get(1, 3), m.get(2, 3), m.get(3, 3));
	}


	/**
	 * Takes the upper left 3x3 submatrix, calculates its inverse and surrounds it by zeros and a 1 in (4, 4).
	 *
	 * Method obtained from http://maths.ucd.ie/courses/math1200/algebra/algebranotes4-3.pdf
	 *
	 * @param m
	 *
	 * @return The normal matrix of view matrix m
	 */
	public static Mat4 inverseMat3(Mat4 m)
	{
		/* Get the nine numbers we need */
		float m11 = m.get(0, 0);
		float m12 = m.get(0, 1);
		float m13 = m.get(0, 2);
		float m21 = m.get(1, 0);
		float m22 = m.get(1, 1);
		float m23 = m.get(1, 2);
		float m31 = m.get(2, 0);
		float m32 = m.get(2, 1);
		float m33 = m.get(2, 2);
		/* Calculate the nine minors */
		float M11 = m22 * m33 - m32 * m23;
		float M12 = m21 * m33 - m31 * m23;
		float M13 = m21 * m32 - m31 * m22;
		float M21 = m12 * m33 - m32 * m13;
		float M22 = m11 * m33 - m31 * m13;
		float M23 = m11 * m32 - m31 * m12;
		float M31 = m12 * m23 - m22 * m13;
		float M32 = m11 * m23 - m21 * m13;
		float M33 = m11 * m22 - m21 * m12;
		/* Build the matrix of cofactors. Transpose to find the adjoint of m. Let's transpose immediately to save time */
		Mat4 mAdjoint = new Mat4(M11, -M21, M31, 0,
				-M12, M22, -M32, 0,
				M13, -M23, M33, 0,
				0, 0, 0, 1);
		/* Multiply by m to get a number times the identity matrix, then extract that number which is the determinant of m */
		float determinant = m.mul(mAdjoint).get(0);
		/* The inverse of m is the adjoint of m divided by the determinant of m */
		return mAdjoint.mul(1 / determinant);
	}
}
