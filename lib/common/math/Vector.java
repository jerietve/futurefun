package common.math;

import java.nio.FloatBuffer;


/**
 * An abstract class that represents any vector.
 *
 * @author Maarten van Meersbergen
 */
public abstract class Vector
{
	protected float v[];
	private final FloatBuffer buf;


	protected Vector(int size)
	{
		this.v = new float[size];
		this.buf = FloatBuffer.wrap(this.v);
		this.buf.rewind();
	}


	/**
	 * Returns the flattened Array associated with this vector.
	 *
	 * @return This matrix as a flat Array.
	 */
	public float[] asArray()
	{
		return this.v;
	}


	/**
	 * Returns the FloatBuffer associated with this vector.
	 *
	 * @return This vector as a FloatBuffer.
	 */
	public FloatBuffer asBuffer()
	{
		this.buf.rewind();
		return this.buf;
	}


	/**
	 * Retrieves the value of the vector at the given index.
	 *
	 * @param i The index.
	 *
	 * @return The value of the vector at index i.
	 */
	public float get(int i)
	{
		return this.v[i];
	}


	/**
	 * Sets the value of the vector at the given index.
	 *
	 * @param i The index.
	 * @param u The new value.
	 */
	public void set(int i, float u)
	{
		this.v[i] = u;
	}


	@Override
	public String toString()
	{
		String result = "(";
		for(int i = 0; i < this.v.length -1; i++)
		{
			result += this.v[i] + ", ";
		}

		return result + this.v[this.v.length - 1] + ")";
	}
}
