package common.math;


/**
 * A class that represents a 2-place vector.
 *
 * @author Thilo Kielmann
 */
public class Vec2 extends Vector
{
	/**
	 * Creates a new vector, initialized to 0.
	 */
	public Vec2()
	{
		super(2);
		this.v[0] = 0f;
		this.v[1] = 0f;
	}


	/**
	 * Creates a new vector with the given values.
	 *
	 * @param x The value to be put in the first position.
	 * @param y The value to be put in the second position.
	 */
	public Vec2(float x, float y)
	{
		super(2);
		this.v[0] = x;
		this.v[1] = y;
	}


	/**
	 * Creates a new vector by copying the given vector.
	 *
	 * @param v The vector to be copied.
	 */
	public Vec2(Vec2 v)
	{
		super(2);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
	}


	/**
	 * Creates a new vector by copying the given 3D-vector and stripping the final position off.
	 *
	 * @param v The vector to be copied.
	 */
	public Vec2(Vec3 v)
	{
		super(2);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
	}


	/**
	 * Creates a new vector by copying the given 4D-vector and stripping the final positions off.
	 *
	 * @param v The vector to be copied.
	 */
	public Vec2(Vec4 v)
	{
		super(2);
		this.v[0] = v.v[0];
		this.v[1] = v.v[1];
	}


	/**
	 * Adds the given vector to the current vector, and returns the result.
	 *
	 * @param u The vector to be added to this vector.
	 *
	 * @return The new vector.
	 */
	public void add(Vec2 u)
	{
		v[0] += u.v[0];
		v[1] += u.v[1];
	}


	public Vec2 addV(Vec2 u)
	{
		add(u);
		return this;
	}


	@Override
	public Vec2 clone()
	{
		return new Vec2(this);
	}


	/**
	 * Divides the current vector with the given scalar.
	 *
	 * @param n The scalar to be divided with.
	 */
	public void div(float f)
	{
		if(f == 0f)
		{
			v[0] = v[1] = 0f;
		}
		final float fn = 1f / f;

		v[0] *= fn;
		v[1] *= fn;
	}


	public Vec2 divV(float f)
	{
		div(f);
		return this;
	}


	@Override
	public boolean equals(Object thatObject)
	{
		if(this == thatObject)
		{
			return true;
		}
		if(!(thatObject instanceof Vec2))
		{
			return false;
		}

		// cast to native object is now safe
		final Vec2 that = (Vec2) thatObject;

		// now a proper field-by-field evaluation can be made
		return ((this.v[0] == that.v[0]) && (this.v[1] == that.v[1]));
	}


	@Override
	public int hashCode()
	{
		final int hashCode = (int) (this.v[0] + 23 * 6833 + this.v[1] + 7 * 7207);
		return hashCode;
	}


	/**
	 * Multiplies the given scalar with this vector.
	 *
	 * @param n The scalar to be multiplied with this one.
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public void mul(float f)
	{
		v[0] *= f;
		v[1] *= f;
	}


	public Vec2 mulV(float f)
	{
		mul(f);
		return this;
	}


	/**
	 * Multiplies the given vector with this vector.
	 *
	 * @param u The vector to be multiplied with this one.
	 */
	public void mul(Vec2 u)
	{
		v[0] *= u.v[0];
		v[1] *= u.v[1];
	}


	public Vec2 mulV(Vec2 u)
	{
		mul(u);
		return this;
	}


	/**
	 * Negates this vector.
	 */
	public void neg()
	{
		v[0] = -v[0];
		v[1] = -v[1];
	}


	public Vec2 negV()
	{
		neg();
		return this;
	}


	/**
	 * Subtracts the given vector from this vector.
	 *
	 * @param u The vector to be subtracted from this one.
	 */
	public void sub(Vec2 u)
	{
		v[0] -= u.v[0];
		v[1] -= u.v[1];
	}


	public Vec2 subV(Vec2 u)
	{
		sub(u);
		return this;
	}


	public void normalize()
	{
		div(length());
	}


	public Vec2 normalizeV()
	{
		normalize();
		return this;
	}


	public float length()
	{
		return (float) Math.sqrt(VectorMath.dot(this, this));
	}
}
