package common.math;

import java.nio.FloatBuffer;
import java.util.List;


/**
 * A library for vector manipulation.
 *
 * @author Maarten van Meersbergen
 */
public class VectorMath
{
	/**
	 * Adds the two vectors, and returns the result.
	 *
	 * @param u The first vector
	 * @param v	The second vector
	 *
	 * @return The new vector.
	 */
	public static Vec4 add(Vec4 u, Vec4 v)
	{
		final Vec4 result = new Vec4();
		result.v[0] = v.v[0] + u.v[0];
		result.v[1] = v.v[1] + u.v[1];
		result.v[2] = v.v[2] + u.v[2];
		result.v[3] = v.v[3] + u.v[3];
		return result;
	}


	/**
	 * Adds the two vectors, and returns the result.
	 *
	 * @param u The first vector
	 * @param v	The second vector
	 *
	 * @return The new vector.
	 */
	public static Vec2 add(Vec2 u, Vec2 v)
	{
		final Vec2 result = new Vec2();
		result.v[0] = v.v[0] + u.v[0];
		result.v[1] = v.v[1] + u.v[1];
		return result;
	}


	/**
	 * Multiplies the scalar and the vector.
	 *
	 * @param f	The number
	 * @param v	The vector
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public static Vec4 mul(Vec4 v, float f)
	{
		final Vec4 result = new Vec4();
		result.v[0] = v.v[0] * f;
		result.v[1] = v.v[1] * f;
		result.v[2] = v.v[2] * f;
		result.v[3] = v.v[3] * f;
		return result;
	}


	/**
	 * Multiplies the scalar and the vector.
	 *
	 * @param f	The number
	 * @param v	The vector
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public static Vec2 mul(Vec2 v, float f)
	{
		final Vec2 result = new Vec2();
		result.v[0] = v.v[0] * f;
		result.v[1] = v.v[1] * f;
		return result;
	}


	/**
	 * Multiplies the two vectors.
	 *
	 * @param u Any vector
	 * @param v	Another vector
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public static Vec4 mul(Vec4 u, Vec4 v)
	{
		final Vec4 result = new Vec4();
		result.v[0] = v.v[0] * u.v[0];
		result.v[1] = v.v[1] * u.v[1];
		result.v[2] = v.v[2] * u.v[2];
		result.v[3] = v.v[3] * u.v[3];
		return result;
	}


	/**
	 * Multiplies the two vectors.
	 *
	 * @param u Any vector
	 * @param v	Another vector
	 *
	 * @return The new Vector, which is a result of the multiplication.
	 */
	public static Vec2 mul(Vec2 u, Vec2 v)
	{
		final Vec2 result = new Vec2();
		result.v[0] = v.v[0] * u.v[0];
		result.v[1] = v.v[1] * u.v[1];
		return result;
	}


	/**
	 * Divides the vector with the scalar.
	 *
	 * @param v	The vector to divide
	 * @param n The scalar to be divided with.
	 *
	 * @return The new Vector, which is a result of the division.
	 */
	public static Vec4 div(Vec4 v, float f)
	{
		if(f == 0f)
		{
			return new Vec4();
		}
		final float fn = 1f / f;

		final Vec4 result = new Vec4();
		result.v[0] = v.v[0] * fn;
		result.v[1] = v.v[1] * fn;
		result.v[2] = v.v[2] * fn;
		result.v[3] = v.v[3] * fn;
		return result;
	}


	/**
	 * Divides the vector with the scalar.
	 *
	 * @param v	The vector to divide
	 * @param n The scalar to be divided with.
	 *
	 * @return The new Vector, which is a result of the division.
	 */
	public static Vec2 div(Vec2 v, float f)
	{
		if(f == 0f)
		{
			return new Vec2();
		}
		final float fn = 1f / f;

		final Vec2 result = new Vec2();
		result.v[0] = v.v[0] * fn;
		result.v[1] = v.v[1] * fn;
		return result;
	}


	/**
	 * Gives the negated vector.
	 *
	 * @return The new negated vector.
	 */
	public static Vec4 neg(Vec4 v)
	{
		final Vec4 result = new Vec4();
		result.v[0] = -v.v[0];
		result.v[1] = -v.v[1];
		result.v[2] = -v.v[2];
		result.v[3] = -v.v[3];
		return result;
	}


	/**
	 * Gives the negated vector.
	 *
	 * @return The new negated vector.
	 */
	public static Vec2 neg(Vec2 v)
	{
		final Vec2 result = new Vec2();
		result.v[0] = -v.v[0];
		result.v[1] = -v.v[1];
		return result;
	}


	/**
	 * Subtracts the second vector from the first vector.
	 *
	 * @param u The vector to be subtracted from.
	 * @param v	The vector to be subtracted from u.
	 *
	 * @return The new Vector, which is u - v.
	 */
	public static Vec4 sub(Vec4 u, Vec4 v)
	{
		final Vec4 result = new Vec4();
		result.v[0] = u.v[0] - v.v[0];
		result.v[1] = u.v[1] - v.v[1];
		result.v[2] = u.v[2] - v.v[2];
		result.v[3] = u.v[3] - v.v[3];
		return result;
	}


	/**
	 * Subtracts the second vector from the first vector.
	 *
	 * @param u The vector to be subtracted from.
	 * @param v	The vector to be subtracted from u.
	 *
	 * @return The new Vector, which is u - v.
	 */
	public static Vec2 sub(Vec2 u, Vec2 v)
	{
		final Vec2 result = new Vec2();
		result.v[0] = u.v[0] - v.v[0];
		result.v[1] = u.v[1] - v.v[1];
		return result;
	}


	/**
	 * Helper method to calculate the cross product of two vectors
	 *
	 * @param u The first vector.
	 * @param v The second vector.
	 *
	 * @return The new vector, which is the cross product of the two vectors.
	 */
	public static Vec3 cross(Vec3 u, Vec3 v)
	{
		return new Vec3(u.v[1] * v.v[2] - u.v[2] * v.v[1], u.v[2] * v.v[0] - u.v[0] * v.v[2], u.v[0] * v.v[1] - u.v[1]
				* v.v[0]);
	}


	/**
	 * Helper method to calculate the cross product of two vectors
	 *
	 * @param u The first vector.
	 * @param v The second vector.
	 *
	 * @return The new vector, which is the cross product of the two vectors.
	 */
	public static Vec4 cross(Vec4 u, Vec4 v)
	{
		return new Vec4(u.v[1] * v.v[2] - u.v[2] * v.v[1], u.v[2] * v.v[0] - u.v[0] * v.v[2], u.v[0] * v.v[1] - u.v[1]
				* v.v[0], 0.0f);
	}


	/**
	 * Helper method to calculate the dot product of two vectors
	 *
	 * @param u The first vector.
	 * @param v The second vector.
	 *
	 * @return The dot product of the two vectors.
	 */
	public static float dot(Vec2 u, Vec2 v)
	{
		return u.v[0] * v.v[0] + u.v[1] * v.v[1];
	}


	/**
	 * Helper method to calculate the dot product of two vectors
	 *
	 * @param u The first vector.
	 * @param v The second vector.
	 *
	 * @return The dot product of the two vectors.
	 */
	public static float dot(Vec3 u, Vec3 v)
	{
		return u.v[0] * v.v[0] + u.v[1] * v.v[1] + u.v[2] * v.v[2];
	}


	/**
	 * Helper method to calculate the dot product of two vectors
	 *
	 * @param u The first vector.
	 * @param v The second vector.
	 *
	 * @return The dot product of the two vectors.
	 */
	public static float dot(Vec4 u, Vec4 v)
	{
		return u.v[0] * v.v[0] + u.v[1] * v.v[1] + u.v[2] * v.v[2] + u.v[3] * v.v[3];
	}


	/**
	 * Calculates the projection of a onto b.
	 *
	 * @param a	The vector to project
	 * @param b	The vector component
	 *
	 * @return
	 */
	public static Vec3 project(Vec3 a, Vec3 b)
	{
		Vec3 bn = normalize(b);
		return bn.mul(dot(a, bn));
	}


	/**
	 * Calculates the projection of a onto b.
	 *
	 * @param a	The vector to project
	 * @param b	The vector component
	 *
	 * @return
	 */
	public static Vec4 project(Vec4 a, Vec4 b)
	{
		Vec4 bn = normalize(b);
		return bn.mulV(dot(a, bn));
	}


	/**
	 * Calculates the projection of a onto the plane given by normal n.
	 *
	 * @param a	The vector to project
	 * @param n	The normal of the plane to project onto
	 *
	 * @return
	 */
	public static Vec3 projectPlane(Vec3 a, Vec3 n)
	{
		// First project a onto n, then subtract that from n
		return a.sub(project(a, n));
	}


	/**
	 * Calculates the projection of a onto the plane given by normal n.
	 *
	 * @param a	The vector to project
	 * @param n	The normal of the plane to project onto
	 *
	 * @return
	 */
	public static Vec4 projectPlane(Vec4 a, Vec4 n)
	{
		// First project a onto n, then subtract that from n
		return sub(a, project(a, n));
	}


	/**
	 * Calculate the angle between the positive x-axis and v
	 *
	 * @param v
	 *
	 * @return The angle in radians
	 */
	public static double directionCosineA(Vec4 v)
	{
		return Math.acos(v.get(0) / v.length());
	}


	/**
	 * Calculate the angle between the positive y-axis and v
	 *
	 * @param v
	 *
	 * @return The angle in radians
	 */
	public static double directionCosineB(Vec4 v)
	{
		return Math.acos(v.get(1) / v.length());
	}


	/**
	 * Calculate the angle between the positive z-axis and v
	 *
	 * @param v
	 *
	 * @return The angle in radians
	 */
	public static double directionCosineC(Vec4 v)
	{
		return Math.acos(v.get(2) / v.length());
	}


	/**
	 * Calculates the angle between two vectors using the dot product.
	 *
	 * @param a
	 * @param b
	 *
	 * @return The angle between a and b in radians [0, 2*PI)
	 */
	public static float angleBetween(Vec3 a, Vec3 b)
	{
		return (float)Math.acos(dot(a, b) / (length(a) * length(b)));
	}


	/**
	 * Helper method to calculate the length of a vector.
	 *
	 * @param u The vector.
	 *
	 * @return The length of the vector.
	 */
	public static float length(Vec3 v)
	{
		return (float) Math.sqrt(VectorMath.dot(v, v));
	}


	/**
	 * Helper method to normalize a vector.
	 *
	 * @param u The vector.
	 *
	 * @return The normal of the vector.
	 */
	public static Vec2 normalize(Vec2 v)
	{
		return div(v, v.length());
	}


	/**
	 * Helper method to normalize a vector.
	 *
	 * @param u The vector.
	 *
	 * @return The normal of the vector.
	 */
	public static Vec3 normalize(Vec3 v)
	{
		return v.div(VectorMath.length(v));
	}


	/**
	 * Helper method to normalize a vector.
	 *
	 * @param u The vector.
	 *
	 * @return The normal of the vector.
	 */
	public static Vec4 normalize(Vec4 v)
	{
		return div(v, v.length());
	}


	/**
	 * Helper method to create a FloatBuffer from an array of floats.
	 *
	 * @param array The array of floats.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer toBuffer(float[] array)
	{
		final FloatBuffer result = FloatBuffer.allocate(array.length);
		result.put(array);
		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The array of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer toBuffer(Point2[] array)
	{
		final FloatBuffer result = FloatBuffer.allocate(array.length * 3);

		for(final Point2 element : array)
		{
			result.put(element.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The array of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer toBuffer(Vec3[] array)
	{
		final FloatBuffer result = FloatBuffer.allocate(array.length * 3);

		for(final Vec3 element : array)
		{
			result.put(element.asBuffer());
		}

		result.rewind();

		return result;
	}


	public static FloatBuffer toBuffer(Vector[] array)
	{
		final FloatBuffer result = FloatBuffer.allocate(array.length * array[0].asArray().length);

		for(final Vector element : array)
		{
			result.put(element.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The array of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer toBuffer(Vec4[] array)
	{
		final FloatBuffer result = FloatBuffer.allocate(array.length * 4);

		for(final Vec4 element : array)
		{
			result.put(element.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The List of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer vec2ListToBuffer(List<Vec2> list)
	{
		final FloatBuffer result = FloatBuffer.allocate(list.size() * 2);

		for(final Vector v : list)
		{
			result.put(v.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The List of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer point2ListToBuffer(List<Point2> list)
	{
		final FloatBuffer result = FloatBuffer.allocate(list.size() * 2);

		for(final Vector v : list)
		{
			result.put(v.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The List of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer vec3ListToBuffer(List<Vec3> list)
	{
		final FloatBuffer result = FloatBuffer.allocate(list.size() * 3);

		for(final Vector v : list)
		{
			result.put(v.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The List of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer point3ListToBuffer(List<Point3> list)
	{
		final FloatBuffer result = FloatBuffer.allocate(list.size() * 3);

		for(final Vector v : list)
		{
			result.put(v.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of vectors.
	 *
	 * @param array The List of vectors.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer vec4ListToBuffer(List<Vec4> list)
	{
		final FloatBuffer result = FloatBuffer.allocate(list.size() * 4);

		for(final Vector v : list)
		{
			result.put(v.asBuffer());
		}

		result.rewind();

		return result;
	}


	/**
	 * Helper method to create a FloatBuffer from an array of points.
	 *
	 * @param array The List of points.
	 *
	 * @return The new FloatBuffer
	 */
	public static FloatBuffer point4ListToBuffer(List<Point4> list)
	{
		final FloatBuffer result = FloatBuffer.allocate(list.size() * 4);

		for(final Vector v : list)
		{
			result.put(v.asBuffer());
		}

		result.rewind();

		return result;
	}
}
