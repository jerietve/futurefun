package common;

import com.jogamp.common.nio.Buffers;
import common.math.Vec4;
import io.Logger;
import java.nio.Buffer;
import java.nio.IntBuffer;
import javax.media.opengl.GL;
import javax.media.opengl.GL3;


/**
 * A class to represent a vertex buffer object.
 *
 * @author Maarten van Meersbergen
 */
public class VertexBufferObject
{
	private final IntBuffer VAOPointer, attribBuffer;
	private IntBuffer indexBuffer;
	private GLSLAttrib[] attribs;
	private int format;
	private int numVertices;
//	private GLSLAttrib indices;
	private Vec4[] vertices;


	/**
	 * Constructor for a vertex buffer object. Creates and initializes the data store but does not yet load it into
	 * graphics memory.
	 *
	 * @param gl		The current GL instance.
	 * @param format	For example GL3.GL_LINES or GL3.GL_TRIANGLE_STRIP
	 * @param vertices	The Vectors describing the vertices of this object
	 * @param attribs	The per-vertex attributes to be stored in this buffer.
	 */
	public VertexBufferObject(GL3 gl, int format, Vec4[] vertices, GLSLAttrib... attribs)
	{
		this(gl, vertices, format, attribs);
	}


	/**
	 * Constructor for a vertex buffer object. Creates and initializes the data store but does not yet load it into
	 * graphics memory.
	 *
	 * @param gl		The current GL instance.
	 * @param vertices	The Vectors describing the vertices of this object
	 * @param format	For example GL3.GL_LINES or GL3.GL_TRIANGLE_STRIP
	 * @param attribs	The per-vertex attributes to be stored in this buffer.
	 */
	public VertexBufferObject(GL3 gl, Vec4[] vertices, int format, GLSLAttrib[] attribs)
	{
		this.attribs = attribs;
		this.vertices = vertices;
		this.format = format;
		this.numVertices = vertices.length;

		// Flush any error
		gl.glGetError();

		this.VAOPointer = Buffers.newDirectIntBuffer(1);
		gl.glGenVertexArrays(1, this.VAOPointer);
		gl.glBindVertexArray(this.VAOPointer.get(0));

		this.attribBuffer = Buffers.newDirectIntBuffer(1);
		gl.glGenBuffers(1, this.attribBuffer);
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.attribBuffer.get(0));

		int size = 0;
		for(final GLSLAttrib attrib : attribs)
		{
			size += attrib.getBuffer().capacity() * attrib.getElementSize();
		}

		gl.glBufferData(GL.GL_ARRAY_BUFFER, size, (Buffer) null, GL.GL_STATIC_DRAW);

		int nextStart = 0;
		for(final GLSLAttrib attrib : attribs)
		{
			gl.glBufferSubData(GL.GL_ARRAY_BUFFER, nextStart, attrib.getBuffer().capacity() * attrib.getElementSize(),
					attrib.getBuffer());
			nextStart += attrib.getBuffer().capacity() * attrib.getElementSize();
		}

		Logger.errorDie(gl.glGetError(), "Could not create VBO");
	}


	/**
	 * Bind the vertex buffer object to be the currently used one.
	 *
	 * @param gl The current GL instance.
	 */
	public void bind(GL3 gl)
	{
		gl.glBindVertexArray(this.VAOPointer.get(0));
		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.attribBuffer.get(0));
	}


	/**
	 * Delete this VBO from Graphics memory.
	 *
	 * @param gl The current GL instance.
	 */
	public void delete(GL3 gl)
	{
		gl.glDeleteVertexArrays(1, this.VAOPointer);
		gl.glDeleteBuffers(1, this.attribBuffer);
	}


	public GLSLAttrib[] getAttribs()
	{
		return this.attribs;
	}


//	/**
//	 * Is this an Indexed Vertex Buffer Object?
//	 *
//	 * @return true if so, false if not
//	 */
//	public boolean indexed()
//	{
//		return indices != null;
//	}
//
//
//	public GLSLAttrib getIndices()
//	{
//		return indices;
//	}


//	public void update(GL3 gl, GLSLAttrib... attribs)
//	{
//		this.attribs = attribs;
//
//		gl.glBindVertexArray(this.VAOPointer.get(0));
//		gl.glBindBuffer(GL.GL_ARRAY_BUFFER, this.attribBuffer.get(0));
//
//		int size = 0;
//		for(final GLSLAttrib attrib : attribs)
//		{
//			size += attrib.getBuffer().capacity() * Buffers.SIZEOF_FLOAT;
//		}
//
//		gl.glBufferData(GL.GL_ARRAY_BUFFER, size, (Buffer) null, GL.GL_STATIC_DRAW);
//
//		int nextStart = 0;
//		for(final GLSLAttrib attrib : attribs)
//		{
//			gl.glBufferSubData(GL.GL_ARRAY_BUFFER, nextStart, attrib.getBuffer().capacity() * Buffers.SIZEOF_FLOAT,
//					attrib.getBuffer());
//			nextStart += attrib.getBuffer().capacity() * Buffers.SIZEOF_FLOAT;
//		}
//	}


	/**
	 * @return the format
	 */
	public int getFormat()
	{
		return format;
	}


	/**
	 * @return The number of vertices
	 */
	public int getNumVertices()
	{
		return numVertices;
	}


	public Vec4[] getVertices()
	{
		return vertices;
	}
}
