package common.shaders;

import com.jogamp.common.nio.Buffers;
import exceptions.CompilationFailedException;
import exceptions.UninitializedException;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.IntBuffer;
import java.util.Scanner;
import javax.media.opengl.GL;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GL3;


/**
 * Java wrapper for any shader.
 *
 * @author Maarten van Meersbergen
 */
public abstract class Shader
{
	private final String filename;
	private final String[] source;
	protected int shaderPointer = -1; // This value is set by creating either a vertex, geometry or


	/**
	 * Generic constructor for shaders. This should not be used directly, but only through the fragment, geometry or
	 * vertex shader constructors.
	 *
	 * @param filename The name of the file in which the source code resides.
	 *
	 * @throws FileNotFoundException Thrown when the file indicated is not found.
	 */
	public Shader(String filename) throws FileNotFoundException
	{
		this.filename = filename;

		// Read file
		final StringBuffer buf = new StringBuffer();
		final File file = new File(filename);
		Scanner scan;
		scan = new Scanner(file);

		while(scan.hasNext())
		{
			buf.append(scan.nextLine());
			buf.append("\n");
		}

		this.source = new String[]
		{
			buf.toString()
		};
	}


	/**
	 * Method to extract the pointer to this shader.
	 *
	 * @return The pointer.
	 *
	 * @throws UninitializedException Is thrown if this method is called before the shader is properly initialized.
	 */
	public int getShaderPointer() throws UninitializedException
	{
		if(this.shaderPointer == -1)
		{
			throw new UninitializedException();
		}
		return this.shaderPointer;
	}


	/**
	 * Method to initialize this shader using the GL instance.
	 *
	 * @param gl The current GL instance.
	 *
	 * @throws CompilationFailedException If the compilation of this shader's source was unsuccesful. A message will be
	 *                                       provided detailing the reason.
	 */
	public void init(GL3 gl) throws CompilationFailedException
	{
		gl.glShaderSource(this.shaderPointer, 1, this.source, (int[]) null, 0);
		gl.glCompileShader(this.shaderPointer);

		final IntBuffer buf = Buffers.newDirectIntBuffer(1);
		gl.glGetShaderiv(this.shaderPointer, GL2ES2.GL_COMPILE_STATUS, buf);

		final int status = buf.get(0);
		if(status == GL.GL_FALSE)
		{
			gl.glGetShaderiv(this.shaderPointer, GL2ES2.GL_INFO_LOG_LENGTH, buf);
			final int logLength = buf.get(0);
			final byte[] reason = new byte[logLength];
			gl.glGetShaderInfoLog(this.shaderPointer, logLength, null, 0, reason, 0);

			throw new CompilationFailedException("Compilation of " + this.filename + " failed, " + new String(reason));
		}
	}


	/**
	 * Delete this shader from the GPU as soon as it is no longer attached to any program.
	 *
	 * @param gl
	 */
	public void delete(GL3 gl)
	{
		gl.glDeleteShader(shaderPointer);
	}
}
