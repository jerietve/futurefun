package common.shaders;

import com.jogamp.common.nio.Buffers;
import common.GLSLAttrib;
import common.math.Matrix;
import common.math.Vector;
import exceptions.UninitializedException;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import javax.media.opengl.GL;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GL3;


/**
 * Java wrapper for a shader program containing 2 to 3 custom shaders (vertex and fragment are mandatory, geometry is
 * optional).
 *
 * @author Maarten van Meersbergen
 */
public class Program
{
	public int pointer;
	private final ArrayList<Shader> shaders;
	private final HashMap<String, Matrix> uniformFloatMatrices;
	private final HashMap<String, Vector> uniformFloatVectors;
	private final HashMap<String, Integer> uniformInts;
	private final HashMap<String, Float> uniformFloats;


	/**
	 * Contructor for a shader program.
	 *
	 */
	public Program()
	{
		this.pointer = 0;
		this.shaders = new ArrayList<Shader>();
		this.uniformFloatMatrices = new HashMap<String, Matrix>();
		this.uniformFloatVectors = new HashMap<String, Vector>();
		this.uniformInts = new HashMap<String, Integer>();
		this.uniformFloats = new HashMap<String, Float>();
	}


	/**
	 * Deletes this program from the Graphics Memory.
	 *
	 * @param gl The current GL instance.
	 */
	public void delete(GL3 gl)
	{
		gl.glDeleteProgram(this.pointer);
	}


	/**
	 * Add a Shader to this program. It will be attached when the program is linked.
	 *
	 * @param gl
	 * @param shader
	 */
	public void addShader(GL3 gl, Shader shader)
	{
		shaders.add(shader);
	}


	/**
	 * Method to detach the shaders from this program.
	 *
	 * @param gl The current GL instance.
	 */
	public void detachShaders(GL3 gl)
	{
		try
		{
			for(Shader shader : shaders)
			{
				gl.glDetachShader(this.pointer, shader.getShaderPointer());
				shader.delete(gl);
			}
		}
		catch(final UninitializedException e)
		{
			System.out.println("Shaders not initialized properly");
			System.exit(0);
		}
	}


	/**
	 * Initializes this program.
	 *
	 * @param gl The current GL instance.
	 */
	public int link(GL3 gl)
	{
		this.pointer = gl.glCreateProgram();
		try
		{
			for(Shader shader : shaders)
			{
				gl.glAttachShader(this.pointer, shader.getShaderPointer());
			}
		}
		catch(final UninitializedException e)
		{
			System.out.println("Shaders not initialized properly");
			System.exit(0);
		}
		gl.glLinkProgram(this.pointer);

		// Check for errors
		final IntBuffer buf = Buffers.newDirectIntBuffer(1);

		gl.glGetProgramiv(this.pointer, GL2ES2.GL_LINK_STATUS, buf);

		if(buf.get(0) == 0)
		{
			System.err.print("Link error");
			printError(gl);
		}

		return this.pointer;
	}


	/**
	 * Links the per-vertex attributes of this program.
	 *
	 * @param gl The current GL instance.
	 * @param attribs Any amount of GLSLAttrib - wrapped attributes.
	 */
	public void linkAttribs(GL3 gl, GLSLAttrib... attribs)
	{
		int nextStart = 0;

		for(final GLSLAttrib attrib : attribs)
		{
			final int ptr = gl.glGetAttribLocation(this.pointer, attrib.getName());
			gl.glVertexAttribPointer(ptr, attrib.getVectorSize(), GL.GL_FLOAT, false, 0, nextStart);
			gl.glEnableVertexAttribArray(ptr);
			//TODO look into glDisableVertexAttribArray
			nextStart += attrib.getBuffer().capacity() * Buffers.SIZEOF_FLOAT;
		}
	}


	/**
	 * Passes a scalar to a uniform variable in a shader program (copy to graphics memory).
	 *
	 * @param gl The current GL instance.
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The scalar value to be passed.
	 */
	public void passUniform(GL3 gl, String pointerNameInShader, float var)
	{
		final int ptr = gl.glGetUniformLocation(this.pointer, pointerNameInShader);

		gl.glUniform1f(ptr, var);
	}


	/**
	 * Passes a scalar to a uniform variable in a shader program (copy to graphics memory).
	 *
	 * @param gl The current GL instance.
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The scalar value to be passed.
	 */
	public void passUniform(GL3 gl, String pointerNameInShader, int var)
	{
		final int ptr = gl.glGetUniformLocation(this.pointer, pointerNameInShader);

		gl.glUniform1i(ptr, var);
	}


	/**
	 * Passes a matrix to a uniform variable in a shader program (copy to graphics memory).
	 *
	 * @param gl The current GL instance.
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The matrix to be passed.
	 */
	public void passUniformMatrix(GL3 gl, String pointerNameInShader, Matrix mat)
	{
		final int ptr = gl.glGetUniformLocation(this.pointer, pointerNameInShader);
		final FloatBuffer var = mat.asBuffer();

		final int matSize = var.capacity();

		if(matSize == 4)
		{
			gl.glUniformMatrix2fv(ptr, 1, true, var);
		}
		else if(matSize == 9)
		{
			gl.glUniformMatrix3fv(ptr, 1, true, var);
		}
		else if(matSize == 16)
		{
			gl.glUniformMatrix4fv(ptr, 1, true, var);
		}
	}


	/**
	 * Passes a vector to a uniform variable in a shader program (copy to graphics memory).
	 *
	 * @param gl The current GL instance.
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The vector to be passed.
	 */
	public void passUniformVector(GL3 gl, String pointerNameInShader, Vector vec)
	{
		final int ptr = gl.glGetUniformLocation(this.pointer, pointerNameInShader);
		final FloatBuffer var = vec.asBuffer();

		final int vecSize = var.capacity();

		if(vecSize == 1)
		{
			gl.glUniform1fv(ptr, 1, var);
		}
		else if(vecSize == 2)
		{
			gl.glUniform2fv(ptr, 1, var);
		}
		else if(vecSize == 3)
		{
			gl.glUniform3fv(ptr, 1, var);
		}
		else if(vecSize == 4)
		{
			gl.glUniform4fv(ptr, 1, var);
		}
	}


	/**
	 * Internal method for error printing.
	 *
	 * @param gl
	 */
	private void printError(GL3 gl)
	{
		final IntBuffer buf = Buffers.newDirectIntBuffer(1);

		gl.glGetProgramiv(this.pointer, GL2ES2.GL_INFO_LOG_LENGTH, buf);

		final int logLength = buf.get(0);
		final ByteBuffer reason = ByteBuffer.wrap(new byte[logLength]);

		gl.glGetProgramInfoLog(this.pointer, logLength, null, reason);

		System.err.println(new String(reason.array()));
	}


	/**
	 * Stages a scalar value to be passed into the Graphics card memory when this program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The scalar to be staged.
	 */
	public void setUniform(String pointerNameInShader, Float var)
	{
		this.uniformFloats.put(pointerNameInShader, var);
	}


	/**
	 * Stages a scalar value to be passed into the Graphics card memory when this program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The scalar to be staged.
	 */
	public void setUniform(String pointerNameInShader, Integer var)
	{
		this.uniformInts.put(pointerNameInShader, var);
	}


	/**
	 * Stages a matrix value to be passed into the Graphics card memory when this program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The matrix (as buffer) to be staged.
	 */
	public void setUniformMatrix(String pointerNameInShader, Matrix var)
	{
		this.uniformFloatMatrices.put(pointerNameInShader, var);
	}


	/**
	 * Stages a vector value to be passed into the Graphics card memory when this program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The vector (as buffer) to be staged.
	 */
	public void setUniformVector(String pointerNameInShader, Vector var)
	{
		this.uniformFloatVectors.put(pointerNameInShader, var);
	}


	/**
	 * Use this program with the geometry to be drawn afterwards. Pass all previously staged variables to the graphics
	 * memory.
	 *
	 * @param gl The current GL instance.
	 *
	 * @throws UninitializedException If this metod is called before this program is properly initialized.
	 */
	public void use(GL3 gl)
	{
		// Check for errors
		final IntBuffer buf = Buffers.newDirectIntBuffer(1);

		gl.glGetProgramiv(this.pointer, GL3.GL_LINK_STATUS, buf);
		gl.glUseProgram(this.pointer);

		if(buf.get(0) == 0)
		{
			System.err.print("Link error");
			printError(gl);
		}

		for(final Entry<String, Matrix> var : this.uniformFloatMatrices.entrySet())
		{
			passUniformMatrix(gl, var.getKey(), var.getValue());
		}

		for(final Entry<String, Vector> var : this.uniformFloatVectors.entrySet())
		{
			passUniformVector(gl, var.getKey(), var.getValue());
		}

		for(final Entry<String, Integer> var : this.uniformInts.entrySet())
		{
			passUniform(gl, var.getKey(), var.getValue());
		}

		for(final Entry<String, Float> var : this.uniformFloats.entrySet())
		{
			passUniform(gl, var.getKey(), var.getValue());
		}
	}
}
