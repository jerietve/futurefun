package common.shaders;

import common.GLSLAttrib;
import common.math.Matrix;
import common.math.Vector;
import exceptions.CompilationFailedException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javax.media.opengl.GL3;


/**
 * Program loader for several shader programs to be used in a single application.
 *
 * @author Maarten van Meersbergen
 */
public class ProgramLoader
{
	private ArrayList<Program> programs;


	/**
	 * Contructor to create an instance of the program loader.
	 */
	public ProgramLoader()
	{
		this.programs = new ArrayList<Program>();
	}


	/**
	 * Delete all programs associate with this loader from graphics memory.
	 *
	 * @param gl The current GL instance.
	 */
	public void cleanup(GL3 gl)
	{
		for(final Program p : this.programs)
		{
			p.detachShaders(gl);
			p.delete(gl);
		}
		this.programs.clear();
	}


	/**
	 * Create a new program, to be associated with this loader. The program is created and initialized.
	 *
	 * @param gl The current GL instance.
	 * @param vs_src The fileName for the source file of the vertex shader.
	 * @param fs_src The fileName for the source file of the fragment shader.
	 *
	 * @return The program that was created.
	 *
	 * @throws FileNotFoundException Thrown if any of the source files are not accessible.
	 * @throws CompilationFailedException Thrown if any of the source code is not compilable. A message detailing the
	 *                                       reason is provided.
	 */
	public Program createProgram(GL3 gl, String vs_src, String fs_src) throws FileNotFoundException,
			CompilationFailedException
	{
		final VertexShader vs = new VertexShader(vs_src);
		vs.init(gl);
		final FragmentShader fs = new FragmentShader(fs_src);
		fs.init(gl);

		final Program program = new Program();
		program.addShader(gl, fs);
		program.addShader(gl, vs);
		program.link(gl);
		this.programs.add(program);

		return program;
	}


	/**
	 * Create a new program, to be associated with this loader. The program is created and initialized.
	 *
	 * @param gl The current GL instance.
	 * @param vs The vertex shader.
	 * @param fs The fragment shader.
	 *
	 * @return The program that was created.
	 */
	public Program createProgram(GL3 gl, Shader... shaders)
	{
		final Program program = new Program();
		for(Shader shader : shaders)
		{
			program.addShader(gl, shader);
		}
		this.programs.add(program);
		program.link(gl);

		return program;
	}


	/**
	 * Delete the specified program from this loader.
	 *
	 * @param gl The current GL instance.
	 * @param program The program to be deleted.
	 */
	public void deleteProgram(GL3 gl, Program program)
	{
		final ArrayList<Program> temp = new ArrayList<Program>();
		for(final Program p : this.programs)
		{
			if(p == program)
			{
				p.detachShaders(gl);
				p.delete(gl);
			}
			else
			{
				temp.add(p);
			}
		}
		this.programs = temp;
	}


	/**
	 * Link the specified attributes with all programs registered with this loader.
	 *
	 * @param gl The current GL instance.
	 * @param attribs Any amount of GLSLAttrib - wrapped attributes.
	 */
	public void linkAttribs(GL3 gl, GLSLAttrib... attribs)
	{
		for(final Program p : this.programs)
		{
			p.linkAttribs(gl, attribs);
		}
	}


	/**
	 * Stages a scalar value in every program registered with this loader, to be passed into the Graphics card memory
	 * when the program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The scalar to be staged.
	 */
	public void setUniform(String pointerNameInShader, float var)
	{
		for(final Program p : this.programs)
		{
			p.setUniform(pointerNameInShader, var);
		}
	}


	/**
	 * Stages a scalar value in every program registered with this loader, to be passed into the Graphics card memory
	 * when the program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The scalar to be staged.
	 */
	public void setUniform(String pointerNameInShader, int var)
	{
		for(final Program p : this.programs)
		{
			p.setUniform(pointerNameInShader, var);
		}
	}


	/**
	 * Stages a matrix in every program registered with this loader, to be passed into the Graphics card memory when the
	 * program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The matrix to be staged.
	 */
	public void setUniformMatrix(String pointerNameInShader, Matrix var)
	{
		for(final Program p : this.programs)
		{
			p.setUniformMatrix(pointerNameInShader, var);
		}
	}


	/**
	 * Stages a vector in every program registered with this loader, to be passed into the Graphics card memory when the
	 * program is used.
	 *
	 * @param pointerNameInShader A string specifying the name of this uniform variable in the shader program.
	 * @param var The vector to be staged.
	 */
	public void setUniformVector(String pointerNameInShader, Vector var)
	{
		for(final Program p : this.programs)
		{
			p.setUniformVector(pointerNameInShader, var);
		}
	}
}
