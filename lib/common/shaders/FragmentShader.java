package common.shaders;

import java.io.FileNotFoundException;
import javax.media.opengl.GL2ES2;
import javax.media.opengl.GL3;
import exceptions.CompilationFailedException;


/**
 * Java wrapper for a fragment shader.
 *
 * @author Maarten van Meersbergen
 */
public class FragmentShader extends Shader
{
	/**
	 * Generic constructor for a fragment shader.
	 *
	 * @param filename The name of the file in which the source code for this shader is stored.
	 *
	 * @throws FileNotFoundException If the file given does not exist or cannot be opened.
	 */
	public FragmentShader(String filename) throws FileNotFoundException
	{
		super(filename);
	}


	@Override
	public void init(GL3 gl) throws CompilationFailedException
	{
		this.shaderPointer = gl.glCreateShader(GL2ES2.GL_FRAGMENT_SHADER);
		super.init(gl);
	}
}
