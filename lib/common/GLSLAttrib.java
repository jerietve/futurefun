package common;

import java.nio.Buffer;
import java.nio.FloatBuffer;


/**
 * A wrapper for per-vertex information.
 *
 * @author Maarten van Meersbergen
 */
public class GLSLAttrib
{
	private final Buffer buffer;
	private final int elementSize;
	private final String name;
	private final int vectorSize;


	/**
	 * Constructor to make a wrapper for per-vertex information.
	 *
	 * @param buffer The actual buffer with the information.
	 * @param pointerNameInShader The name of this attribute in the shader.
	 * @param vectorSize The per-vector size of the vectors in the buffer.
	 */
	public GLSLAttrib(Buffer buffer, int elementSize, String pointerNameInShader, int vectorSize)
	{
		this.buffer = buffer;
		this.elementSize = elementSize;
		this.name = pointerNameInShader;
		this.vectorSize = vectorSize;
	}


	/**
	 * @return the buffer
	 */
	public Buffer getBuffer()
	{
		return this.buffer;
	}


	/**
	 * @return the name
	 */
	public String getName()
	{
		return this.name;
	}


	/**
	 * @return the vectorSize
	 */
	public int getVectorSize()
	{
		return this.vectorSize;
	}

	
	public int getElementSize()
	{
		return elementSize;
	}
}
