package io;


/**
 * This class has some general functions available to help with pressed keys. It does not handle the result of the
 * input. For that you have to be in Cat, F16, FreeCamera etc.
 *
 * @author jeroen
 */
public class KeyboardInput
{
	private int keyCode;
	private long lastAction;


	/**
	 * Create a new KeyboardInput object.
	 *
	 * @param keyCode The key code associated with this object
	 */
	public KeyboardInput(int keyCode)
	{
		this.keyCode = keyCode;
		lastAction = -1;
	}


	/**
	 * Has it been more than millis milliseconds since this key was last used?
	 *
	 * @param millis
	 * @return
	 */
	public boolean itsBeen(long millis)
	{
		return System.currentTimeMillis() - lastAction > millis;
	}


	/**
	 * Touch this
	 */
	public void actionTaken()
	{
		lastAction = System.currentTimeMillis();
	}


	/**
	 * @return the keyCode
	 */
	public int getKeyCode()
	{
		return keyCode;
	}


	/**
	 * @return The time when this key was last used
	 */
	public long getLastAction()
	{
		return lastAction;
	}


	@Override
	public boolean equals(Object object)
	{
		return getClass() == object.getClass() && ((KeyboardInput)object).getKeyCode() == keyCode;
	}


	@Override
	public int hashCode()
	{
		return keyCode;
	}
}
