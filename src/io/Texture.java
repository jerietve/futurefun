package io;
/*
 * Texture Class by Ben van Werkhoven
 * Written for the Computer Graphics course 2009
 * Rewritten for the Computer Graphics course of 2011
 *
 * This class reads a texture and provides you with
 * the information you need to provide OpenGL with
 * to create the actual texture.
 *
 * Please use it in the following way:
 *
 * somewhere in your program, probably init():
 * Texture myTexture = new Texture("textures/my_texture.jpg", GL3.GL_TEXTURE0);
 *
 * only in your init() method:
 * myTexture.init(gl);
 *
 * in display():
 * myTexture.bind(gl, shaderprogram, "my_texture");
 *
 * GL3.GL_TEXTURE0 is the glMultiTexUnit(), which is used
 * when using multiple textures in your program. If you want
 * to use multiple textures in the same shader program you
 * should use different values for these textures.
 *
 * The texture is copied to the graphics memory when the
 * init() method of this class is called. Make sure you
 * don't call init() when you don't have to.
 *
 * You are free and encouraged to make any changes or to
 * completely rewrite this class to your liking.
 *
 * Ben
 *
 */

import com.jogamp.common.nio.Buffers;
import common.shaders.Program;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;
import javax.imageio.ImageIO;
import javax.media.opengl.GL3;


public class Texture
{
	private ByteBuffer pixelBuffer;
	private int width;
	private int height;
	private int gLMultiTexUnit;
	private IntBuffer pointer;


	public Texture(String filename, int gLMultiTexUnit)
	{
		this.gLMultiTexUnit = gLMultiTexUnit;

		BufferedImage bi = null;
		try
		{
			bi = ImageIO.read(new FileInputStream(filename));
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}

		int x = 0,
				y = 0,
				w = bi.getWidth(),
				h = bi.getHeight();

		width = w;
		height = h;

		int[] pixels = new int[w * h];
		PixelGrabber pg = new PixelGrabber(bi, x, y, w, h, pixels, 0, w);
		try
		{
			pg.grabPixels();
		}
		catch(InterruptedException e)
		{
			System.err.println("interrupted waiting for pixels!");
			return;
		}

		pixelBuffer = ByteBuffer.allocate(w * h * 4);

		for(int j = 0; j < h; j++)
		{
			for(int i = 0; i < w; i++)
			{
				pixelBuffer.put((byte) ((pixels[j * w + i] >> 16) & 0xff)); //red
				pixelBuffer.put((byte) ((pixels[j * w + i] >> 8) & 0xff)); //green
				pixelBuffer.put((byte) ((pixels[j * w + i]) & 0xff)); //blue
				pixelBuffer.put((byte) ((pixels[j * w + i] >> 24) & 0xff)); //alpha
			}
		}

		pixelBuffer.flip();
	}


	/**
	 * Call this method _once_ per texture and only in init()
	 *
	 * @param gl The current GL instance
	 */
	public void init(GL3 gl)
	{
		//Tell OpenGL we want to use textures
		gl.glEnable(GL3.GL_TEXTURE_2D);
		gl.glActiveTexture(this.gLMultiTexUnit);

		//Create a Texture Object
		pointer = Buffers.newDirectIntBuffer(1);
		gl.glGenTextures(1, pointer);

		//Tell OpenGL that this texture is 2D and we want to use it
		gl.glBindTexture(GL3.GL_TEXTURE_2D, pointer.get(0));

		//Tell OpenGL what type of filtering to use
		gl.glTexParameteri(GL3.GL_TEXTURE_2D, GL3.GL_TEXTURE_WRAP_S, GL3.GL_REPEAT);
		gl.glTexParameteri(GL3.GL_TEXTURE_2D, GL3.GL_TEXTURE_WRAP_T, GL3.GL_REPEAT);
		gl.glTexParameteri(GL3.GL_TEXTURE_2D, GL3.GL_TEXTURE_MIN_FILTER, GL3.GL_LINEAR);
		gl.glTexParameteri(GL3.GL_TEXTURE_2D, GL3.GL_TEXTURE_MAG_FILTER, GL3.GL_LINEAR);

		// Specifies the alignment requirements for the start of each pixel row in memory.
		gl.glPixelStorei(GL3.GL_UNPACK_ALIGNMENT, 1);

		//Specify a 2D texture to OpenGL and transfer it to the graphics memory
		gl.glTexImage2D(GL3.GL_TEXTURE_2D, //target
				0, //mipmap level
				GL3.GL_RGBA, //internal format
				this.width, //width
				this.height, //height
				0, //border
				GL3.GL_RGBA, //external format
				GL3.GL_UNSIGNED_BYTE, //type
				this.pixelBuffer);				//data
	}


	public void bind(GL3 gl, Program program, String string)
	{
		gl.glEnable(GL3.GL_TEXTURE_2D);
		gl.glActiveTexture(this.gLMultiTexUnit);

		//bind the texture to the active multiTexUnit
		gl.glBindTexture(GL3.GL_TEXTURE_2D, this.pointer.get(0));

		program.setUniform(string, this.getGLMultiTexNumber());
	}


	public void setPixelBuffer(ByteBuffer pixelBuffer)
	{
		this.pixelBuffer = pixelBuffer;
	}


	public ByteBuffer getPixelBuffer()
	{
		return pixelBuffer;
	}


	public void setWidth(int width)
	{
		this.width = width;
	}


	public int getWidth()
	{
		return width;
	}


	public void setHeight(int height)
	{
		this.height = height;
	}


	public int getHeight()
	{
		return height;
	}


	public int getGLMultiTexNumber()
	{
		int result = -1;

		switch(this.gLMultiTexUnit)
		{
			case GL3.GL_TEXTURE0:
				result = 0;
				break;
			case GL3.GL_TEXTURE1:
				result = 1;
				break;
			case GL3.GL_TEXTURE2:
				result = 2;
				break;
			case GL3.GL_TEXTURE3:
				result = 3;
				break;
			case GL3.GL_TEXTURE4:
				result = 4;
				break;
			case GL3.GL_TEXTURE5:
				result = 5;
				break;
			case GL3.GL_TEXTURE6:
				result = 6;
				break;
			case GL3.GL_TEXTURE7:
				result = 7;
				break;
			case GL3.GL_TEXTURE8:
				result = 8;
				break;
			case GL3.GL_TEXTURE9:
				result = 9;
				break;
			case GL3.GL_TEXTURE10:
				result = 10;
				break;
			case GL3.GL_TEXTURE11:
				result = 11;
				break;
			case GL3.GL_TEXTURE12:
				result = 12;
				break;
			case GL3.GL_TEXTURE13:
				result = 13;
				break;
			case GL3.GL_TEXTURE14:
				result = 14;
				break;
		}

		return result;
	}
}