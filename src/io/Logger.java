package io;

import com.jogamp.common.nio.Buffers;
import java.nio.IntBuffer;
import javax.media.opengl.GL3;
import javax.media.opengl.glu.GLU;

/**
 * This class contains some really, really basic functions for printing information. These functions are here for future
 * expansion.
 *
 * @author jeroen
 */
public class Logger
{
	/**
	 * Check if there is an error, and iff so print the message and die.
	 *
	 * @param errorCode
	 * @param message
	 */
	public static void errorDie(int errorCode, String message)
	{
		if(errorCode != GL3.GL_NO_ERROR)
		{
			GLU glu = new GLU();
			System.out.println("ERROR: " + message + ":\n" + glu.gluErrorString(errorCode));
			System.exit(1);
		}
	}


	/**
	 * Simply prints the message to System.err
	 *
	 * @param message
	 */
	public static void printError(String message)
	{
		System.err.println("Error: " + message);
	}


	/**
	 * Print some information about the currently used graphics card that is useful to determine what we can and can't
	 * do.
	 *
	 * @param gl
	 */
	public static void printConstants(GL3 gl)
	{
		IntBuffer buf = Buffers.newDirectIntBuffer(1);
		gl.glGetIntegerv(GL3.GL_MAX_PATCH_VERTICES, buf);
		System.out.println("GL_MAX_PATCH_VERTICES: " + buf.get(0));
		gl.glGetIntegerv(GL3.GL_MAX_TESS_GEN_LEVEL, buf);
		System.out.println("GL_MAX_TESS_GEN_LEVEL: " + buf.get(0));
//		gl.glGetIntegerv(GL3.GL_PATCH_DEFAULT_OUTER_LEVEL, buf);
//		System.out.println("GL_PATCH_DEFAULT_OUTER_LEVEL: " + buf.get(0));
//		gl.glGetIntegerv(GL3.GL_PATCH_DEFAULT_INNER_LEVEL, buf);
//		System.out.println("GL_PATCH_DEFAULT_INNER_LEVEL: " + buf.get(0));
		gl.glGetIntegerv(GL3.GL_MAX_SUBROUTINES, buf);
		System.out.println("GL_MAX_SUBROUTINES: " + buf.get(0));
		gl.glGetIntegerv(GL3.GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS, buf);
		System.out.println("GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS: " + buf.get(0));
		gl.glGetIntegerv(GL3.GL_MAX_UNIFORM_BUFFER_BINDINGS, buf);
		System.out.println("GL_MAX_UNIFORM_BUFFER_BINDINGS: " + buf.get(0));
	}
}
