package io;
/*
 * SGFLoader Class by Ben van Werkhoven
 * Written for the Computer Graphics course 2009
 * Rewritten for the Computer Graphics course of 2011
 *
 * This class is made to have very basic functionality
 * of reading the SGF format and producing a vertexBufferObject
 * for each file located in the directory of the
 * model. The LoadModel() method returns an HashMap containing
 * the filenames and VertexBufferObjects.
 *
 * You are required to change this class to add textures
 * to the models. Feel free to make any changes or to
 * completely rewrite this class to your liking.
 *
 * How to use this class:
 *
 * The static LoadModel() method this class provides can be used in
 * the following way in, most likely, your init() method:
 *
 * 	HashMap<String, VertexBufferObject> vboHashMap = null;
 * 	try {
 * 		vboHashMap = SGFLoader.LoadModel(gl, shaderprogram, "models/cat");
 * 	} catch (IOException e) {
 * 		e.printStackTrace();
 * 	}
 *
 * To draw the model you can add the following to display():
 *
 *  shaderprogram.use(gl);
 *	for (Map.Entry<String, VertexBufferObject> entry : vboHashMap.entrySet()) {
 *	    String key = entry.getKey();
 *	    entry.getValue().bind(gl);
 *	    gl.glDrawArrays(GL3.GL_TRIANGLES, 0, SGFLoader.getNumVertices(key));
 *	}
 *
 * IMPORTANT:
 * Since this class returns vertexBufferObjects you only have to call
 * LoadModel() _once_ for every model you wish to use in your program.
 * Remember that you can use the same vertexBufferObjects to draw multiple
 * instances of the same model at different locations in your scene.
 *
 */

import com.jogamp.common.nio.Buffers;
import common.GLSLAttrib;
import common.VertexBufferObject;
import common.math.Vec4;
import common.shaders.Program;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.HashMap;
import javax.media.opengl.*;


public class SGFLoader
{
	static HashMap<String, Integer> numVertices = new HashMap<String, Integer>(50);


	public static HashMap<String, VertexBufferObject> LoadModel(GL3 gl, Program program, String dirname) throws IOException
	{
		File dir = new File(dirname);
		File[] sgfFiles = dir.listFiles();

		if(sgfFiles == null)
		{
			throw new IOException("path " + dirname + " does not exist");
		}

		FloatBuffer pointsBuf;
		FloatBuffer colorBuf;
		FloatBuffer normalBuf;
		Vec4[] vertices;

		HashMap<String, VertexBufferObject> hmap = new HashMap<String, VertexBufferObject>(20);

		//for each file in this directory
		for(File f : sgfFiles)
		{
			// skip directories and hidden files...
			if(f.isDirectory() || (f.getName() != null && f.getName().startsWith(".")))
			{
				continue;
			}

			String fileNameWithoutExtension;
			if(f.getName().lastIndexOf('.') == -1) fileNameWithoutExtension = f.getName();
			else fileNameWithoutExtension = f.getName().substring(0, f.getName().lastIndexOf('.'));

			//Create a reader for the file
			BufferedReader in = new BufferedReader(new FileReader(f));

			String line;
			//Read the first line
			while((line = in.readLine()) != null)
			{
				String[] splitString = line.split(" ");

				String drawMode = splitString[0];
				int n = Integer.parseInt(splitString[1]);

				pointsBuf = FloatBuffer.allocate(n * 4);
				colorBuf = FloatBuffer.allocate(n * 4);
				normalBuf = FloatBuffer.allocate(n * 3);
				vertices = new Vec4[n];

				//restricting to only triangles saves us the need to provide a mechanism to store the draw mode
				if(!drawMode.equals("TRIANGLES"))
				{
					System.err.println("Error: OpenGL draw modes other than TRIANGLES are no longer support by SGFLoader\n");
					System.exit(1);
				}

				//Read the color line
				line = in.readLine();
				splitString = line.split(" ");
				float color[] = new float[4];
				color[0] = Float.parseFloat(splitString[0]);
				color[1] = Float.parseFloat(splitString[1]);
				color[2] = Float.parseFloat(splitString[2]);
				color[3] = Float.parseFloat(splitString[3]);

				pointsBuf.clear();
				colorBuf.clear();
				normalBuf.clear();

				//Read n lines consisting of 6 floats vertex_x vertex_y vertex_z normal_x normal_y normal_z
				float vertex[] = new float[4];
				float normal[] = new float[3];

				for(int i = 0; i < n; i++)
				{
					line = in.readLine();
					splitString = line.split(" ");

					vertex[0] = Float.parseFloat(splitString[0]);
					vertex[1] = Float.parseFloat(splitString[1]);
					vertex[2] = Float.parseFloat(splitString[2]);
					vertex[3] = 1.0f;

					vertices[i] = new Vec4(vertex[0], vertex[1], vertex[2], vertex[3]);

					normal[0] = Float.parseFloat(splitString[3]);
					normal[1] = Float.parseFloat(splitString[4]);
					normal[2] = Float.parseFloat(splitString[5]);

					pointsBuf.put(vertex);
					colorBuf.put(color);
					normalBuf.put(normal);
				}

				pointsBuf.rewind();
				colorBuf.rewind();
				normalBuf.rewind();

				final GLSLAttrib points = new GLSLAttrib(pointsBuf, Buffers.SIZEOF_FLOAT, "vPosition", 4);
				final GLSLAttrib colors = new GLSLAttrib(colorBuf, Buffers.SIZEOF_FLOAT, "vColor", 4);
				final GLSLAttrib normals = new GLSLAttrib(normalBuf, Buffers.SIZEOF_FLOAT, "vNormal", 3);

				//add new VBO to hashmap
				VertexBufferObject vbo = new VertexBufferObject(gl, GL3.GL_TRIANGLES, vertices, points, colors, normals);
				hmap.put(fileNameWithoutExtension, vbo);

				//initialize VBO for this shader
				vbo.bind(gl);
				program.linkAttribs(gl, points, colors, normals);

				//store the number of vertices for this file
				numVertices.put(fileNameWithoutExtension, n);
			}
		}

		return hmap;
	}


	//return the number of vertices in the vertex buffer object
	//for a particular filename
	public static int getNumVertices(String str)
	{
		return numVertices.get(str);
	}
}
