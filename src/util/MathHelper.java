package util;

import common.math.Point2;
import common.math.Vec2;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * helper class containing some mathematical functions.
 *
 * @author jeroen
 */
public class MathHelper
{
	/**
	 * Convert a value from radians to degrees.
	 *
	 * @param angleRad
	 * @return
	 */
	public static float rad2deg(float angleRad)
	{
		return (float)(angleRad / Math.PI * 180);
	}


	/**
	 * Convert a value from degrees to radians
	 *
	 * @param angleDeg
	 * @return
	 */
	public static float deg2rad(float angleDeg)
	{
		return (float)(angleDeg / 180.0 * Math.PI);
	}


	/**
	 * Find out if number is a power of two
	 *
	 * @param number
	 * @return
	 */
	public static boolean isPowerOfTwo(int number)
	{
		return (number & number - 1) == 0;
	}


	/**
	 * Calculates the upper left point in a rectangle aligned with the x and z-axes spanning from a to b.
	 *
	 * @param a An (x, z) vector
	 * @param b An (x, z) vector
	 *
	 * @return
	 */
	public static Point2 upperLeft(Vec2 a, Vec2 b)
	{
		return new Point2(Math.min(a.get(0), b.get(0)), Math.min(a.get(1), b.get(1)));
	}


	/**
	 * Calculates the lower right point in a rectangle aligned with the x and z-axes spanning from a to b.
	 *
	 * @param a An (x, z) vector
	 * @param b An (x, z) vector
	 *
	 * @return
	 */
	public static Point2 lowerRight(Vec2 a, Vec2 b)
	{
		return new Point2(Math.max(a.get(0), b.get(0)), Math.max(a.get(1), b.get(1)));
	}


	/**
	 * Calculates the lower left point in a rectangle aligned with the x and z-axes spanning from a to b.
	 *
	 * @param a An (x, z) vector
	 * @param b An (x, z) vector
	 *
	 * @return
	 */
	public static Point2 lowerLeft(Vec2 a, Vec2 b)
	{
		return new Point2(Math.min(a.get(0), b.get(0)), Math.max(a.get(1), b.get(1)));
	}


	/**
	 * Calculates the upper right point in a rectangle aligned with the x and z-axes spanning from a to b.
	 *
	 * @param a An (x, z) vector
	 * @param b An (x, z) vector
	 *
	 * @return
	 */
	public static Point2 upperRight(Vec2 a, Vec2 b)
	{
		return new Point2(Math.max(a.get(0), b.get(0)), Math.min(a.get(1), b.get(1)));
	}


	/**
	 * A faster method to floor a double to an integer
	 *
	 * Taken from http://webstaff.itn.liu.se/~stegu/simplexnoise/
	 *
	 * @param x The number to floor
	 *
	 * @return The integer part of x
	 */
	public static int fastFloor(double x)
	{
		int xi = (int) x;
		return x < xi ? xi - 1 : xi;
	}


	/**
	 * A faster method to ceil a double to an integer
	 *
	 * Inspired by http://webstaff.itn.liu.se/~stegu/simplexnoise/
	 *
	 * @param x The number to ceil
	 *
	 * @return x ceiled to an integer
	 */
	public static int fastCeil(double x)
	{
		int xi = (int) x;
		return x < xi ? xi : xi + 1;
	}


	/**
	 * A faster method to floor a double to an integer
	 *
	 * Taken from http://webstaff.itn.liu.se/~stegu/simplexnoise/
	 *
	 * @param x The number to floor
	 *
	 * @return The integer part of x
	 */
	public static int fastFloor(float x)
	{
		int xi = (int) x;
		return x < xi ? xi - 1 : xi;
	}


	/**
	 * @return The amount of milliseconds we're into this day at this moment
	 */
	public static int millisecondOfDay()
	{
		Date now = new Date();
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(now);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		int millisecond = calendar.get(Calendar.MILLISECOND);
		return hour * 3600000 + minute * 60000 + second * 1000 + millisecond;
	}


	/**
	 * @return How much percent of this day has passed
	 */
	public static float percentageOfDay(float minutesPerDay)
	{
		return ((float)millisecondOfDay() / 1000 / 60) % minutesPerDay;
	}
}
