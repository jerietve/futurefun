package util;

import common.math.Vec4;

/**
 * Some color presets
 */
public class Color
{
	public static Vec4 black = new Vec4(0.0f, 0.0f, 0.0f, 1.0f);
	public static Vec4 red = new Vec4(1.0f, 0.0f, 0.0f, 1.0f);
	public static Vec4 yellow = new Vec4(1.0f, 1.0f, 0.0f, 1.0f);
	public static Vec4 green = new Vec4(0.0f, 1.0f, 0.0f, 1.0f);
	public static Vec4 blue = new Vec4(0.0f, 0.0f, 1.0f, 1.0f);
	public static Vec4 magenta = new Vec4(1.0f, 0.0f, 1.0f, 1.0f);
	public static Vec4 white = new Vec4(1.0f, 1.0f, 1.0f, 1.0f);
	public static Vec4 cyan = new Vec4(0.0f, 1.0f, 1.0f, 1.0f);
	public static Vec4 t_black = new Vec4(0.0f, 0.0f, 0.0f, 0.2f);
	public static Vec4 t_red = new Vec4(1.0f, 0.0f, 0.0f, 0.2f);
	public static Vec4 t_yellow = new Vec4(1.0f, 1.0f, 0.0f, 0.2f);
	public static Vec4 t_green = new Vec4(0.0f, 1.0f, 0.0f, 0.2f);
	public static Vec4 t_blue = new Vec4(0.0f, 0.0f, 1.0f, 0.2f);
	public static Vec4 t_magenta = new Vec4(1.0f, 0.0f, 1.0f, 0.2f);
	public static Vec4 t_white = new Vec4(1.0f, 1.0f, 1.0f, 0.2f);
	public static Vec4 t_cyan = new Vec4(0.0f, 1.0f, 1.0f, 0.2f);
}
