package util;

import common.VertexBufferObject;
import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Vec4;
import java.util.ArrayList;

/**
 * A bounding box for a model
 *
 * @author jeroen
 */
public class BoundingBox
{
	private float xMin, xMax, yMin, yMax, zMin, zMax;	// Bounding values
	// The vectors describing the vertices. U/D for yMax/yMin. L/R for xMin/xMax. F/B for zMin/zMax
	private Vec4 LDF, LUF, RDF, RUF, LDB, LUB, RDB, RUB;
	VertexBufferObject vbo;


	/**
	 * Generates a bounding box around the vertices in the array.
	 *
	 * @param vertices An array of vertices. At least one, or you're getting a broken BoundingBox.
	 */
	public BoundingBox(Vec4[] vertices)
	{
		if(vertices.length < 1) return;

		/* Initialize all max and min values to the first vertex in order to make sure min < max, so we can speed up the
		 * rest of the process. */
		xMin = vertices[0].get(0);
		xMax = vertices[0].get(0);
		yMin = vertices[0].get(1);
		yMax = vertices[0].get(1);
		zMin = vertices[0].get(2);
		zMax = vertices[0].get(2);

		/* Go through all vertices */
		Vec4 vertex;
		for(int i = 1; i < vertices.length; i++)
		{
			vertex = vertices[i];
			if(vertex.get(0) < xMin) xMin = vertex.get(0);
			else if(vertex.get(0) > xMax) xMax = vertex.get(0);
			if(vertex.get(1) < yMin) yMin = vertex.get(1);
			else if(vertex.get(1) > yMax) yMax = vertex.get(1);
			if(vertex.get(2) < zMin) zMin = vertex.get(2);
			else if(vertex.get(2) > zMax) zMax = vertex.get(2);
		}
		regenerateVertices();
	}


	/**
	 * Generates a BoundingBox containing all BoundingBoxes from the ArrayList.
	 *
	 * @param boxes At least one BoundingBox, or you're getting a broken BoundingBox.
	 */
	public BoundingBox(ArrayList<BoundingBox> boxes)
	{
		if(boxes == null || boxes.isEmpty()) return;
		xMin = yMin = zMin = Float.POSITIVE_INFINITY;
		xMax = yMax = zMax = Float.NEGATIVE_INFINITY;

		for(BoundingBox box : boxes)
		{
			if(box == null) continue;
			if(box.getxMin() < xMin) xMin = box.getxMin();
			if(box.getxMax() > xMax) xMax = box.getxMax();
			if(box.getyMin() < yMin) yMin = box.getyMin();
			if(box.getyMax() > yMax) yMax = box.getyMax();
			if(box.getzMin() < zMin) zMin = box.getzMin();
			if(box.getzMax() > zMax) zMax = box.getzMax();
		}
		regenerateVertices();
	}


	/**
	 * Recalculate the vectors from the x, y and z values
	 */
	private void regenerateVertices()
	{
		LDF = new Vec4(xMin, yMin, zMin, 1);
		LUF = new Vec4(xMin, yMax, zMin, 1);
		RDF = new Vec4(xMax, yMin, zMin, 1);
		RUF = new Vec4(xMax, yMax, zMin, 1);
		LDB = new Vec4(xMin, yMin, zMax, 1);
		LUB = new Vec4(xMin, yMax, zMax, 1);
		RDB = new Vec4(xMax, yMin, zMax, 1);
		RUB = new Vec4(xMax, yMax, zMax, 1);
		//TODO delete
		vbo = null;
	}


	/**
	 * Reset the x, y and z values using the vectors
	 */
	public void recalculateExtremes()
	{
		xMin = LDB.get(0);
		xMax = RDB.get(0);
		yMin = LDB.get(1);
		yMax = LUB.get(1);
		zMin = LUF.get(2);
		zMax = LUB.get(2);
		//TODO delete
		vbo = null;
	}


	/**
	 * Move the bounding box in y-direction, saving a complete recalculation
	 *
	 * @param dist The distance to move, positive for moving toward y positive infinity
	 */
	public void moveY(float dist)
	{
		yMin += dist;
		yMax += dist;
		regenerateVertices();
	}


	/**
	 * Move the bounding box in y-direction, saving a complete recalculation
	 *
	 * @param dist The distance to move, positive for moving toward x positive infinity
	 */
	public void moveX(float dist)
	{
		xMax += dist;
		xMin += dist;
		regenerateVertices();
	}


	/**
	 * Move the bounding box in y-direction, saving a complete recalculation
	 *
	 * @param dist The distance to move, positive for moving toward z positive infinity
	 */
	public void moveZ(float dist)
	{
		zMax += dist;
		zMin += dist;
		regenerateVertices();
	}


	/**
	 * Move the bounding box by a vector.
	 *
	 * @param vec
	 */
	public void move(Vec4 vec)
	{
		/* Calculate the translation matrix */
		Mat4 m = MatrixMath.translate(vec);
		/* Multiply the matrix with each of the vertices */
		LDF = m.mul(LDF);
		LUF = m.mul(LUF);
		RDF = m.mul(RDF);
		RUF = m.mul(RUF);
		LDB = m.mul(LDB);
		LUB = m.mul(LUB);
		RDB = m.mul(RDB);
		RUB = m.mul(RUB);
		recalculateExtremes();
	}


	/**
	 * Set the VertexBufferObject
	 *
	 * @param vbo
	 */
	public void setVertexBufferObject(VertexBufferObject vbo)
	{
		this.vbo = vbo;
	}


	/**
	 * @return The VertexBufferObject of this BoundingBox, will be null if not set
	 */
	public VertexBufferObject getVertexBufferObject()
	{
		return vbo;
	}


	/**
	 * @return A vertex array containing the eight corner points of this bounding box arranged for lines drawing
	 */
	public Vec4[] getVertexArrayLines()
	{
		Vec4[] result = new Vec4[24];
		result[0] = LDF;
		result[1] = LDB;
		result[2] = LDF;
		result[3] = LUF;
		result[4] = LDF;
		result[5] = RDF;
		result[6] = LUF;
		result[7] = RUF;
		result[8] = LUF;
		result[9] = LUB;
		result[10] = LUB;
		result[11] = LDB;
		result[12] = LUB;
		result[13] = RUB;
		result[14] = RUB;
		result[15] = RUF;
		result[16] = RUB;
		result[17] = RDB;
		result[18] = RDB;
		result[19] = LDB;
		result[20] = RDB;
		result[21] = RDF;
		result[22] = RDF;
		result[23] = RUF;
		return result;
	}


	/**
	 * @return the xMin
	 */
	public float getxMin()
	{
		return xMin;
	}


	/**
	 * @return the xMax
	 */
	public float getxMax()
	{
		return xMax;
	}


	/**
	 * @return the yMin
	 */
	public float getyMin()
	{
		return yMin;
	}


	/**
	 * @return the yMax
	 */
	public float getyMax()
	{
		return yMax;
	}


	/**
	 * @return the zMin
	 */
	public float getzMin()
	{
		return zMin;
	}


	/**
	 * @return the zMax
	 */
	public float getzMax()
	{
		return zMax;
	}


	@Override
	public String toString()
	{
		return "Bounding box: x: (" + xMin + ", " + xMax + "), y: (" + yMin + ", " + yMax + "), z: (" + zMin + ", " + zMax + ")";
	}
}
