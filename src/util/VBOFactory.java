package util;

import com.jogamp.common.nio.Buffers;
import common.GLSLAttrib;
import common.VertexBufferObject;
import common.math.Vec4;
import common.math.Vector;
import common.math.VectorMath;
import java.nio.FloatBuffer;
import javax.media.opengl.GL3;

/**
 * This class helps create some VertexBufferObjects from other objects, and presents functions for for example creating
 * a simply cube. It also has one nice function that can save a lot of repetition in converting arrays of stuff into a
 * VertexBufferObject.
 *
 * @author jeroen
 */
public class VBOFactory
{
	/**
	 * Create a VertexBufferObject for a BoundingBox
	 *
	 * @param gl
	 * @param box	The BoundingBox
	 * @param color	The color every vertex will have
	 * @return
	 */
	public static VertexBufferObject createVBO(GL3 gl, BoundingBox box, Vec4 color)
	{
		Vec4[] vertices = box.getVertexArrayLines();

		Vec4[] colors = new Vec4[vertices.length];
		for(int i = 0; i < vertices.length; i++)
		{
			colors[i] = color;
		}

		return putInVBO(gl, GL3.GL_LINES, new String[] {"vPosition", "vColor"}, vertices, colors);
	}


	/**
	 * Create a simple cube.
	 *
	 * @param gl
	 * @param edgeLength
	 * @param color
	 * @return
	 */
	public static VertexBufferObject cube(GL3 gl, float edgeLength, Vec4 color)
	{
		float k = edgeLength / 2;
		Vec4 LDF = new Vec4(-k, -k, -k, 1);
		Vec4 LUF = new Vec4(-k, k, -k, 1);
		Vec4 RDF = new Vec4(k, -k, -k, 1);
		Vec4 RUF = new Vec4(k, k, -k, 1);
		Vec4 LDB = new Vec4(-k, -k, k, 1);
		Vec4 LUB = new Vec4(-k, k, k, 1);
		Vec4 RDB = new Vec4(k, -k, k, 1);
		Vec4 RUB = new Vec4(k, k, k, 1);
		Vec4[] vertices = new Vec4[] {LDF, LUF, RDF, RUF, RDB, RUB, LDB, LUB, LDF, LUF, LUF, RUF, LUB, RUB, RUB, RDB, RDB, LDB, RDF, LDF};
		Vec4[] colors = new Vec4[vertices.length];
		for(int i = 0; i < colors.length; i++)
		{
			colors[i] = color;
		}
		return putInVBO(gl, GL3.GL_TRIANGLE_STRIP, new String[] {"vPosition", "vColor"}, vertices, colors);
	}


	/**
	 * Creates a VertexBufferObject using the values in the vector arrays. The names of this data in GLSL should be
	 * provided in attribNames in the same order. The first vector array _must_ be the vertex positions.
	 *
	 * @param gl
	 * @param format
	 * @param attribNames
	 * @param vectorArrays The vertex locations, plus zero or more arrays of vectors of the same length
	 * @return
	 */
	private static VertexBufferObject putInVBO(GL3 gl, int format, String[] attribNames, Vector[]... vectorArrays)
	{
		GLSLAttrib[] attribs = new GLSLAttrib[vectorArrays.length];
		for(int i = 0; i < vectorArrays.length; i++)
		{
			FloatBuffer buffer = VectorMath.toBuffer(vectorArrays[i]);
			attribs[i] = new GLSLAttrib(buffer, Buffers.SIZEOF_FLOAT, attribNames[i], vectorArrays[i][0].asArray().length);
		}

		return new VertexBufferObject(gl, (Vec4[])vectorArrays[0], format, attribs);
	}
}
