/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package scene;

import common.VertexBufferObject;
import common.math.Mat4;
import common.math.Point3;
import common.math.Vec4;
import common.shaders.Program;

/**
 * An F16 should function as the root node for an F16 tree of Models. At the moment, it doesn't do anything but assemble.
 *
 * @author jeroen
 */
public class F16 extends Model
{
	private Model body, wings, cockpit, afterburner, rockets, bomb, tailfin, tailwings;


	public F16(Program program, VertexBufferObject vbo, Mat4 modelM, Model body, Model wings, Model cockpit,
			Model afterburner, Model rockets, Model bomb, Model tailfin, Model tailwings)
	{
		super(program, vbo, modelM);

		init(Vec4.Z_AXIS_N, Vec4.Y_AXIS_N, new Point3());

		/* Assemble the F16 */
		// Body
		this.body = body;
		this.addChild(body);
		// Cockpit
		this.cockpit = cockpit;
		body.addChild(cockpit);
		// Afterburner
		this.afterburner = afterburner;
		body.addChild(afterburner);
		// Wings
		this.wings = wings;
		body.addChild(wings);
		// Rockets
		this.rockets = rockets;
		wings.addChild(rockets);
		// Bomb
		this.bomb = bomb;
		body.addChild(bomb);
		// Tailfin
		this.tailfin = tailfin;
		body.addChild(tailfin);
		// Tailwings
		this.tailwings = tailwings;
		body.addChild(tailwings);
	}
}
