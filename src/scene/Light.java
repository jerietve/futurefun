package scene;

import com.jogamp.common.nio.Buffers;
import common.VertexBufferObject;
import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Vec4;
import common.math.VectorMath;
import common.shaders.Program;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import javax.media.opengl.GL3;
import util.VBOFactory;

/**
 * Contains information on a light in the scene, such as its luminosity and position, and can help setting these values
 * in the GPU.
 *
 * @author jeroen
 */
public class Light
{
	private Vec4 position;
	private Vec4 ambientCoefficient;
	private Vec4 diffuseCoefficient;
	private Vec4 specularCoefficient;
	public int index;


	/**
	 * Create a new light
	 *
	 * @param position	The position in world coordinates
	 * @param ambient	The ambient luminosity
	 * @param diffuse	The diffuse luminosity
	 * @param specular	The specular luminosity
	 */
	public Light(Vec4 position, Vec4 ambient, Vec4 diffuse, Vec4 specular)
	{
		this.position = position;
		ambientCoefficient = ambient;
		diffuseCoefficient = diffuse;
		specularCoefficient = specular;
		this.index = -1;
	}


	/**
	 * Send this light's data to the GPU. This method requires the index to have been set to the value of an otherwise
	 * unbound uniform block index.
	 *
	 * @param gl
	 * @param program
	 */
	public void setUniforms(GL3 gl, Program program)
	{
		/* Get the index of the light properties uniform block in this program */
		int lightPropertiesBlockIndex = gl.glGetUniformBlockIndex(program.pointer, "LightPropertiesBlock[" + index + "]");
		/* Bind it to binding point 0 */
		gl.glUniformBlockBinding(program.pointer, lightPropertiesBlockIndex, index);
		/* Create a buffer to store the data */
		IntBuffer buffer = Buffers.newDirectIntBuffer(1);
		gl.glGenBuffers(1, buffer);
		gl.glBindBuffer(GL3.GL_UNIFORM_BUFFER, buffer.get(0));
		/* Put the data in the buffer */
		FloatBuffer data = FloatBuffer.allocate(4 * 4);
		data.put(position.asBuffer());
		data.put(ambientCoefficient.asBuffer());
		data.put(diffuseCoefficient.asBuffer());
		data.put(specularCoefficient.asBuffer());
		data.rewind();
		gl.glBufferData(GL3.GL_UNIFORM_BUFFER, 4 * 4 * Buffers.SIZEOF_FLOAT, data, GL3.GL_DYNAMIC_DRAW);
		gl.glBindBufferBase(GL3.GL_UNIFORM_BUFFER, index, buffer.get(0));
	}


	/**
	 * Sets the ambient, diffuse and specular color for this light to color
	 *
	 * @param color
	 */
	public void setColor(Vec4 color)
	{
		ambientCoefficient = color.clone();
		diffuseCoefficient = color.clone();
		specularCoefficient = color;
	}


	/**
	 * Returns a VertexBufferObject which is a simple 0.1f cube centered at the origin. This must be multiplied by the
	 * model matrix of a light in order to get the light to render in the right position. The color is determined by the
	 * average of the ambient, diffuse and specular coefficients of the light.
	 *
	 * @param gl	The current GL
	 *
	 * @return A VertexBufferObject which is a simple 0.1f cube centered at the origin
	 */
	public VertexBufferObject getVBO(GL3 gl)
	{
		return VBOFactory.cube(gl, 0.1f, (VectorMath.add(ambientCoefficient, diffuseCoefficient).addV(specularCoefficient)).divV(3));
	}


	/**
	 * @return The model matrix used to draw this light at the right position in world coordinates.
	 */
	public Mat4 getModelMatrix()
	{
		return MatrixMath.translate(position);
	}
}
