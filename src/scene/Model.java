package scene;

import common.VertexBufferObject;
import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Point3;
import common.math.Vec4;
import common.math.VectorMath;
import common.shaders.Program;
import io.KeyboardInput;
import io.Logger;
import io.Texture;
import java.util.ArrayList;
import java.util.Set;
import javax.media.opengl.GL3;
import util.BoundingBox;
import util.Color;
import util.VBOFactory;

/**
 * Generic class for all Models that require no special actions. It should be extended by models that require such
 * actions. Its children are all Models.
 *
 * @author jeroen
 */
public class Model extends TreeNode
{
	protected Vec4 front;				// Unit vector aligned with the front direction of the model
	protected Vec4 up;					// Unit vector aligned with the up direction for the model
	protected Vec4 left;				// Unit vector aligned with left direction of the model
	protected Point3 rotationPoint;		// The point around which all rotations around the axes take place
	protected boolean initialised;		// Has this Model been initialised?
	private BoundingBox personalBoundingBox;	// This Model's bounding box, including only this Model's VBO
	private BoundingBox aggregateBoundingBox;	// This Model's bounding box, including its complete children tree
	private boolean personalBoundingBoxIsValid;	// Is the personal bounding box (still) valid?
	private boolean aggregateBoundingBoxIsValid;// Is the aggregate bounding box (still) valid?
	private boolean drawBoundingBoxes;			// Kinda speaks for itself doesn't it
	private boolean movedHorizontallySinceLastDraw;
	private Texture texture;


	/**
	 * Create a new Model.
	 *
	 * @param program
	 * @param vbo
	 * @param modelM	The matrix that translates this model to the correct position in its parent's space (could be
	 *					world space)
	 */
	public Model(Program program, VertexBufferObject vbo, Mat4 modelM)
	{
		super(program, vbo, modelM);
		aggregateBoundingBoxIsValid = false;
		personalBoundingBoxIsValid = false;
		drawBoundingBoxes = false;
		movedHorizontallySinceLastDraw = false;
		initialised = false;
	}


	/**
	 * Initialize this Model, setting its front and up vector, and the point it should be rotated about in its own
	 * frame.
	 *
	 * @param front
	 * @param up
	 * @param rotationPoint
	 */
	public void init(Vec4 front, Vec4 up, Point3 rotationPoint)
	{
		this.front = VectorMath.normalize(front);
		this.up = VectorMath.normalize(up);
		left = VectorMath.cross(up, front).normalizeV();
		this.rotationPoint = rotationPoint;
		initialised = true;
	}


	/**
	 * @return true if this Model was initialized, false otherwise
	 */
	public boolean initialised()
	{
		return initialised;
	}


	/**
	 * Move this Model to its defined front
	 *
	 * @param dist The distance to move
	 */
	public void moveForward(float dist)
	{
		Vec4 v = VectorMath.mul(front, dist);
		modelMatrix = modelMatrix.mul(MatrixMath.translate(v));
		modelMatrixChanged = true;
		movedHorizontallySinceLastDraw = true;
		moveBoundingBox(v);
	}


	/**
	 * Move this Model to its defined back
	 *
	 * @param dist The distance to move
	 */
	public void moveBack(float dist)
	{
		moveForward(-dist);
	}


	/**
	 * Move this Model to its defined left
	 *
	 * @param dist The distance to move
	 */
	public void moveLeft(float dist)
	{
		Vec4 v = VectorMath.mul(left, dist);
		modelMatrix = modelMatrix.mul(MatrixMath.translate(v));
		modelMatrixChanged = true;
		movedHorizontallySinceLastDraw = true;
		moveBoundingBox(v);
	}


	/**
	 * Move this Model to its defined right
	 *
	 * @param dist The distance to move
	 */
	public void moveRight(float dist)
	{
		moveLeft(dist * -1);
	}


	/**
	 * Move this Model to its defined up
	 *
	 * @param dist The distance to move
	 */
	public void moveUp(float dist)
	{
		Vec4 v = VectorMath.mul(up, dist);
		modelMatrix = modelMatrix.mul(MatrixMath.translate(v));
		modelMatrixChanged = true;
		moveBoundingBox(getModelMatrix().mul(v));
	}


	/**
	 * Move this Model to its defined down
	 *
	 * @param dist The distance to move
	 */
	public void moveDown(float dist)
	{
		moveUp(dist * -1);
	}


	/**
	 * Rotate the model -angle around the front axis
	 *
	 * @param angle Angle in radians
	 */
	public void rotateCW(float angle)
	{
		rotateCCW(angle * -1);
	}


	/**
	 * Rotate the model angle around the front axis
	 *
	 * @param angle Angle in radians
	 */
	public void rotateCCW(float angle)
	{
		modelMatrix = modelMatrix.mul(MatrixMath.translate(rotationPoint));
		modelMatrix = modelMatrix.mul(MatrixMath.rotate(angle, front));
		modelMatrix = modelMatrix.mul(MatrixMath.translate(rotationPoint.mul(-1)));
		modelMatrixChanged = true;
		invalidateBoundingBox();
	}


	/**
	 * Rotate the model angle around the up axis
	 *
	 * @param angle  Angle in radians
	 */
	public void rotateLeft(float angle)
	{
		modelMatrix = modelMatrix.mul(MatrixMath.translate(rotationPoint));
		modelMatrix = modelMatrix.mul(MatrixMath.rotate(angle, up));
		modelMatrix = modelMatrix.mul(MatrixMath.translate(rotationPoint.mul(-1)));
		modelMatrixChanged = true;
		invalidateBoundingBox();
	}


	/**
	 * Rotate the model -angle around the up axis
	 *
	 * @param angle  Angle in radians
	 */
	public void rotateRight(float angle)
	{
		rotateLeft(angle * -1);
	}


	/**
	 * Returns the bounding box of this model and the complete child tree together.
	 *
	 * @return
	 */
	protected BoundingBox getBoundingBox()
	{
		if(!aggregateBoundingBoxIsValid) recalculateAggregateBoundingBox();
		return aggregateBoundingBox;
	}


	/**
	 * Returns the BoundingBox for only this Model's VBO.
	 *
	 * @return
	 */
	private BoundingBox getPersonalBoundingBox()
	{
		if(!personalBoundingBoxIsValid) recalculatePersonalBoundingBox();
		return personalBoundingBox;
	}


	/**
	 * @return A list of the bounding box of this model and the complete child tree.
	 */
	private ArrayList<BoundingBox> getBoundingBoxesBubbleDown()
	{
		ArrayList<BoundingBox> result = new ArrayList<BoundingBox>();
		if(aggregateBoundingBoxIsValid)
		{
			/* In case the aggregate one is valid, there's no need for further child recursion (because it already
			 * includes all children) */
			result.add(aggregateBoundingBox);
		}
		else
		{
			result.add(getPersonalBoundingBox());
			for(TreeNode child : children)
			{
				result.addAll(((Model)child).getBoundingBoxesBubbleDown());
			}
		}
		return result;
	}


	/**
	 * Invalidates the BoundingBoxes of this model and its complete child tree.
	 */
	protected void invalidateBoundingBox()
	{
		personalBoundingBoxIsValid = aggregateBoundingBoxIsValid = false;
		invalidateBoundingBoxesBubbleDown();
		invalidateBoundingBoxesBubbleUp();
	}


	/**
	 * Walks up the Model tree and invalidates the aggregate bounding boxes
	 */
	private void invalidateBoundingBoxesBubbleUp()
	{
		aggregateBoundingBoxIsValid = false;
		if(parent != null && parent instanceof Model) ((Model)parent).invalidateBoundingBoxesBubbleUp();
	}


	/**
	 * Invalidates this Model's BoundingBox and that of its complete child tree
	 */
	private void invalidateBoundingBoxesBubbleDown()
	{
		personalBoundingBoxIsValid = false;
		aggregateBoundingBoxIsValid = false;
		for(TreeNode child : children)
		{
			((Model)child).invalidateBoundingBox();
		}
	}


	/**
	 * Recalculates the BoundingBox for this Model including the complete child tree
	 */
	private void recalculateAggregateBoundingBox()
	{
		ArrayList<BoundingBox> list = new ArrayList<BoundingBox>();
		if(!personalBoundingBoxIsValid) recalculatePersonalBoundingBox();
		list.add(personalBoundingBox);
		for(TreeNode child : children)
		{
			list.addAll(((Model)child).getBoundingBoxesBubbleDown());
		}
		aggregateBoundingBox = new BoundingBox(list);
		aggregateBoundingBoxIsValid = true;
	}


	/**
	 * Recalculates the BoundingBox for this Model's VBO only
	 */
	private void recalculatePersonalBoundingBox()
	{
		/* Sadface.. convert all vertices of this model to world coordinates */
		if(vbo != null)
		{
			Vec4[] vertices = vbo.getVertices();
			Vec4[] adjustedVertices = new Vec4[vertices.length];
			Mat4 totalM = getModelMatrix();
			for(int i = 0; i < vertices.length; i++)
			{
				adjustedVertices[i] = totalM.mul(vertices[i]);
			}
			personalBoundingBox = new BoundingBox(adjustedVertices);
		}
		personalBoundingBoxIsValid = true;
	}


	/**
	 * Move this Model's personal and aggregate BoundingBox and those of all its children
	 *
	 * @param direction	The box will move with this vector
	 */
	private void moveBoundingBox(Vec4 vec)
	{
		if(parent != null && parent instanceof Model) ((Model)parent).invalidateBoundingBoxesBubbleUp();
		if(personalBoundingBox != null)	personalBoundingBox.move(vec);
		if(aggregateBoundingBox != null) aggregateBoundingBox.move(vec);
		for(TreeNode child : children)
		{
			((Model)child).moveBoundingBoxesBubbleDown(vec);
		}
	}


	/**
	 * Calls moveBoundingBoxes on this Model and on its complete child trees
	 *
	 * @param direction	The boxes will move with this vector
	 */
	private void moveBoundingBoxesBubbleDown(Vec4 vec)
	{
		if(personalBoundingBox != null)	personalBoundingBox.move(vec);
		if(aggregateBoundingBox != null) aggregateBoundingBox.move(vec);
		for(TreeNode child : children)
		{
			((Model)child).moveBoundingBoxesBubbleDown(vec);
		}
	}


	/**
	 * Check the heighest point underneath the aggregate bounding box of this model, and make sure the bottom of the
	 * bounding boxes is moved there.
	 *
	 * @param debug Print debug information?
	 */
	public void adjustHeightToTerrain(boolean debug)
	{
		/* If the bounding box is still valid, there is no need to adjust */
		//TODO this assumes the model is always on the ground
		if(!aggregateBoundingBoxIsValid || movedHorizontallySinceLastDraw)
		{
			movedHorizontallySinceLastDraw = false;
			BoundingBox box = getBoundingBox();
			Terrain terrain = getTerrainAncestor();
			if(terrain.getHeightmap().completelyCovers(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax()))
			{
				if(debug) System.out.println("Completely on one terrain");
				/* We only need to deal with one terrain */
				float newHeight = terrain.getHeightmap().getHighestPoint(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax(), debug);
				moveUp(newHeight - box.getyMin());
				return;
			}
			else if(terrain.getHeightmap().partlyCovers(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax()))
			{
				/* We're still partly over the ancestor terrain. Check all other terrains to find out if we're partly
				 * over any other ones, and then use all of them to get the new height */
				ArrayList<Terrain> terrainsUnderBox = new ArrayList<Terrain>();
				for(TreeNode t : getRoot().getChildren())
				{
					if(((Terrain)t).getHeightmap().partlyCovers(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax()))
					{
						terrainsUnderBox.add((Terrain)t);
					}
				}

				if(debug) System.out.println("On " + terrainsUnderBox.size() + " terrains");

				float newHeight = Float.NEGATIVE_INFINITY;
				for(Terrain t : terrainsUnderBox)
				{
					newHeight = Math.max(newHeight, t.getHeightmap().getHighestPoint(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax(), debug));
				}
				moveUp(newHeight - box.getyMin());
				return;
			}
			else
			{
				/* We're no longer over our ancestor terrain. Time to move to another part of the tree. */
				for(TreeNode t : getRoot().getChildren())
				{
					if(((Terrain)t).getHeightmap().partlyCovers(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax()))
					{
						if(debug) System.out.println("Moving to a new terrain");
						/* Found a terrain! Jump here, and restart */
						getRoot().addRearrangement(new TreeNode[] {this, this.parent, t});
						this.parent = t;
						movedHorizontallySinceLastDraw = true;	// Set to ensure recalculation on the next line
						adjustHeightToTerrain(debug);
						return;
					}
				}
				/* No terrain here.. A terrain should have been created.. */
			}
			Logger.printError("Trying to adjust height of model to terrain where there is no terrain");
		}
	}


	/**
	 * Turns bounding boxes on if they were off and the other way around. This model's complete subtree will get the
	 * same value as this model.
	 */
	public void toggleBoundingBoxes()
	{
		drawBoundingBoxes(!drawBoundingBoxes);
	}


	/**
	 * Enables the drawing of bounding boxes for this model and all its children
	 *
	 * @param bool True to turn drawing on, false to turn it off
	 */
	public void drawBoundingBoxes(boolean bool)
	{
		drawBoundingBoxes = bool;
		for(TreeNode child : children)
		{
			((Model)child).drawBoundingBoxes(bool);
		}
	}


	@Override
	public void prepareForDraw(GL3 gl, Mat4 viewM, boolean debug)
	{
		/* Tell the shader if this model should be textured or not */
		program.setUniform("textured", texture == null ? 0 : 1);

		/* Debugging (using) bounding boxes? */
		if(drawBoundingBoxes)
		{
			//Please mind that in the current implementation the bounding boxes are not removed from the GPU, making
			//this a very wasteful practice. Because it's only meant for debugging, this won't be fixed
			if(this instanceof Cat) return;
			if(this instanceof F16) return;
			getBoundingBox();
			if(personalBoundingBox != null)
			{
				if(personalBoundingBox.getVertexBufferObject() == null)
				{
					personalBoundingBox.setVertexBufferObject(VBOFactory.createVBO(gl, personalBoundingBox, Color.red));
				}
				// Draw this node's bounding box
				VertexBufferObject boxVbo = personalBoundingBox.getVertexBufferObject();
				boxVbo.bind(gl);
				program.linkAttribs(gl, boxVbo.getAttribs());
				program.setUniformMatrix("viewMatrix", viewM);
				program.setUniformMatrix("modelMatrix", new Mat4());
				program.setUniform("isBoundingBox", 1);
				program.use(gl);
				gl.glDrawArrays(boxVbo.getFormat(), 0, boxVbo.getNumVertices());
				program.setUniform("isBoundingBox", 0);
			}
			if(aggregateBoundingBox != null)
			{
				if(aggregateBoundingBox.getVertexBufferObject() == null)
				{
					aggregateBoundingBox.setVertexBufferObject(VBOFactory.createVBO(gl, aggregateBoundingBox, Color.blue));
				}
				// Draw this node's bounding box
				VertexBufferObject boxVbo = aggregateBoundingBox.getVertexBufferObject();
				boxVbo.bind(gl);
				program.linkAttribs(gl, boxVbo.getAttribs());
				program.setUniformMatrix("viewMatrix", viewM);
				program.setUniform("isBoundingBox", 1);
				program.use(gl);
				gl.glDrawArrays(boxVbo.getFormat(), 0, boxVbo.getNumVertices());
				program.setUniform("isBoundingBox", 0);
			}
		}
	}


	/**
	 * Add a model as child tree of this model. Models can only have other models as children.
	 *
	 * @param model The Model to be this Model's first child
	 */
	@Override
	public void addChild(TreeNode model)
	{
		if(model instanceof Model)
		{
			super.addChild(model);
		}
		else Logger.printError("Models can only have other Models as children.");
	}


	/**
	 * Redefines new front direction (does NOT turn the model)
	 *
	 * @param front The new front direction
	 */
	public void setFront(Vec4 front)
	{
		this.front = VectorMath.normalize(front);
		this.left = VectorMath.cross(up, front).normalizeV();
	}


	/**
	 * Redefines the up direction (does NOT turn the model)
	 *
	 * @param up The new up direction
	 */
	public void setUp(Vec4 up)
	{
		this.up = VectorMath.normalize(up);
		this.left = VectorMath.cross(up, front).normalizeV();
	}


	/**
	 * Set the Texture of this Model
	 *
	 * @param texture
	 */
	public void setTexture(Texture texture)
	{
		this.texture = texture;
	}


	/**
	 * @return A normalized vector pointing to the front direction of this model instance in world coordinates
	 */
	public Vec4 getForwardDirection()
	{
		return getModelMatrix().mul(front).normalizeV();
	}


	/**
	 * @return A normalized vector pointing in the up direction of this model instance in world coordinates
	 */
	public Vec4 getUpDirection()
	{
		return getModelMatrix().mul(up).normalizeV();
	}


	public void handleKeyReleased(int keyCode)
	{
		/* Extending class implementation specific */
	}


	public void handleKeyboardInput(Set<KeyboardInput> pressedKeys, boolean ctrlPressed, boolean shiftPressed)
	{
		/* Extending class implementation specific */
	}
}
