package scene;

import common.VertexBufferObject;
import common.math.Mat4;
import common.math.MatrixMath;
import common.shaders.Program;
import io.Logger;
import io.Texture;
import java.util.HashMap;
import java.util.Map;
import util.MathHelper;

/**
 * Builds Models (Cats, F16s, etc) from the output of the SGFLoader.
 *
 * @author jeroen
 */
public class ModelBuilder
{
	/**
	 * Takes the output from the SGFLoader and builds a Cat from it
	 *
	 * @param catParts Put in some cat parts
	 * @param program
	 * @return A cat comes out, like magic
	 */
	public static Cat buildCat(HashMap<String, VertexBufferObject> catParts, Program program, Texture fur)
	{
		final int NUM_PARTS = 11;

		if(catParts.size() != NUM_PARTS)
		{
			Logger.printError("Not enough parts provided to build cat");
			return null;
		}

		Model body = null, leftLeg = null, rightLeg = null, leftHand = null, rightHand = null, tail = null,
				leftEye = null, rightEye = null, nose = null, teeth = null, whiskers = null;


		/* Find all body parts in the hashmap and initialize them */
		String partName;
		for(Map.Entry<String, VertexBufferObject> entry : catParts.entrySet())
		{
			partName = entry.getKey();
			if(partName.equalsIgnoreCase("body")) body = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("leg_left")) leftLeg = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("leg_right")) rightLeg = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("hand_left")) leftHand = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("hand_right")) rightHand = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("tail")) tail = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("eye_left")) leftEye = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("eye_right")) rightEye = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("nose")) nose = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("teeth")) teeth = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("whiskers")) whiskers = new Model(program, entry.getValue(), new Mat4());
			else
			{
				Logger.printError("Unknown part found while assembling cat: " + partName);
				return null;
			}
		}

		leftLeg.setTexture(fur);
		rightLeg.setTexture(fur);
		leftHand.setTexture(fur);
		rightHand.setTexture(fur);
		body.setTexture(fur);
		tail.setTexture(fur);

		return new Cat(program, null, MatrixMath.rotationX(MathHelper.deg2rad(-90)), body, leftLeg, rightLeg, leftHand,
				rightHand, tail, leftEye, rightEye, nose, teeth, whiskers, true);
	}


	/**
	 * Takes the output from the SGFLoader and builds an F16 from it
	 *
	 * @param f16Parts
	 * @param program
	 * @return
	 */
	public static F16 buildF16(HashMap<String, VertexBufferObject> f16Parts, Program program)
	{
		final int NUM_PARTS = 8;

		if(f16Parts.size() != NUM_PARTS)
		{
			Logger.printError("Not enough parts provided to build cat");
			return null;
		}

		Model body = null, wings = null, cockpit = null, afterburner = null, rockets = null, bomb = null,
				tailfin = null, tailwings = null;

		String partName;
		for(Map.Entry<String, VertexBufferObject> entry : f16Parts.entrySet())
		{
			partName = entry.getKey();
			if(partName.equalsIgnoreCase("body")) body = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("wings")) wings = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("cockpit")) cockpit = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("afterburner")) afterburner = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("rockets")) rockets = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("bomb")) bomb = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("tailfin")) tailfin = new Model(program, entry.getValue(), new Mat4());
			else if(partName.equalsIgnoreCase("tailwings")) tailwings = new Model(program, entry.getValue(), new Mat4());
		}

		return new F16(program, null, new Mat4(), body, wings, cockpit, afterburner, rockets, bomb, tailfin, tailwings);
	}
}
