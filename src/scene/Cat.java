package scene;

import common.VertexBufferObject;
import common.math.Mat4;
import common.math.Point3;
import common.math.Vec4;
import common.shaders.Program;
import io.KeyboardInput;
import java.awt.event.KeyEvent;
import java.util.Set;
import javax.media.opengl.GL3;
import util.MathHelper;

/**
 * The Cat TreeNode should function as the root node of a cat model.
 *
 * @author jeroen
 */
public class Cat extends Model
{
	private final float RAYMANIZE_DISTANCE = 0.2f;
	private final double ROTATION_SPEED = MathHelper.deg2rad(45) / 1000;

	/* Body parts */
	private Model body, leftLeg, rightLeg, leftHand, rightHand, tail, leftEye, rightEye, nose, teeth, whiskers;

	/* State */
	/* Waving */
	private boolean waving = false, stoppingWave = false;
	private double waveAmplitude, wavePeriod, wavePhase;
	private long lastWaveMovement;
	/* Walking */
	private boolean walking = false, stoppingWalk = false;
	private double walkSpeed, walkPhase, walkPeriod;
	private long lastWalkMovement;
	/* Rotating */
	private boolean rotatingLeft, rotatingRight;
	private long lastTurnAction;
	private float rotationMultiplier = 1;

	/**
	 * Creates a new cat. The names are pretty self-explanatory. If rayman is set to true, the cat will be raymanized.
	 * If you can not guess know what that means, you should not be writing games.
	 */
	public Cat(Program program, VertexBufferObject vbo, Mat4 modelM, Model body, Model leftLeg, Model rightLeg,
			Model leftHand, Model rightHand, Model tail, Model leftEye, Model rightEye, Model nose,
			Model teeth, Model whiskers, boolean rayman)
	{
		super(program, vbo, modelM);

		// The cat model is built front up, but it should be pointed in the negative z-dir
		init(Vec4.Y_AXIS_N, Vec4.Z_AXIS_N, new Point3());

		/* Assemble the cat */
		// Body
		this.body = body;
		addChild(body);
		// Legs
		this.leftLeg = leftLeg;
		leftLeg.init(front, up, new Point3());
		body.addChild(leftLeg);
		this.rightLeg = rightLeg;
		rightLeg.init(front, up, new Point3());
		body.addChild(rightLeg);
		// Hands
		this.leftHand = leftHand;
		leftHand.init(front, up, new Point3(-0.18f, 0, -0.05f));
		leftHand.moveRight(0.05f);
		body.addChild(leftHand);
		this.rightHand = rightHand;
		rightHand.init(front, up, new Point3(0.18f, 0, -0.05f));
		rightHand.moveLeft(0.05f);
		body.addChild(rightHand);
		// Tail
		this.tail = tail;
		tail.init(front, up, new Point3());
		body.addChild(tail);
		// Face
		this.leftEye = leftEye;
		body.addChild(leftEye);
		this.rightEye = rightEye;
		body.addChild(rightEye);
		this.nose = nose;
		body.addChild(nose);
		this.teeth = teeth;
		body.addChild(teeth);
		this.whiskers = whiskers;
		nose.addChild(whiskers);


		/* State */
		walkPhase = Math.PI / 2;
		wavePhase = 0;

		if(rayman)
		{
			raymanize();
			/* Move the rotation point for the arms back */
			leftHand.rotationPoint = new Point3(0, 0, -0.05f);
			rightHand.rotationPoint = new Point3(0, 0, -0.05f);
		}
	}


	/**
	 * Default wave with a period of one second and an amplitude of 30 degrees
	 */
	public void startWaving()
	{
		startWaving(1000, MathHelper.deg2rad(30));
	}


	/**
	 * Starts moving both arms up and down. If the cat is waving already, this has no effect.
	 *
	 * @param period	The amount of time in milliseconds for the cats arms to move a complete cycle
	 * @param amplitude	The maximum angle to move the arms up and down
	 */
	public void startWaving(double period, double amplitude)
	{
		if(!waving)
		{
			waving = true;
			wavePeriod = period;
			waveAmplitude = amplitude;
			wavePhase = 0;
			lastWaveMovement = System.currentTimeMillis();
		}
		else
		{
			waving = true;
			stoppingWave = false;
		}
	}


	/**
	 * Stop waving once the hands reach the neutral position
	 */
	public void stopWaving()
	{
		waving = false;
		stoppingWave = true;
	}


	/**
	 * Start waving if not waving, stop waving if waving
	 */
	public void toggleWave()
	{
		if(!waving) startWaving();
		else stopWaving();
	}


	/**
	 * Start walking forward with a speed of 1 meter per second and move the feet max 40cm apart
	 */
	public void startWalking()
	{
		startWalking(1, 0.4);
	}


	/**
	 * Start walking forward.
	 *
	 * @param speed	The forward speed in meters per second
	 * @param amplitude	The maximum distance between the feet during the walk in meters
	 */
	public void startWalking(double speed, double amplitude)
	{
		walkSpeed = speed / 1000;
		walkPeriod = 2 * amplitude / walkSpeed;

		if(!walking && !stoppingWalk)
		{
			walking = true;
			// milliseconds
			walkPhase = Math.PI / 2;
			lastWalkMovement = System.currentTimeMillis();
		}
		else
		{
			stoppingWalk = false;
			walking = true;
		}
	}


	/**
	 * Stop walking the next time the feet reach the neutral position
	 */
	public void stopWalking()
	{
		walking = false;
		stoppingWalk = true;
	}


	/**
	 * Start walking if not walking, stop walking otherwise
	 */
	public void toggleWalk()
	{
		if(!walking) startWalking();
		else stopWalking();
	}


	/**
	 * Explode the limbs of the cat away from its body, making it look a lot better, and kinda like Rayman.
	 */
	private void raymanize()
	{
		leftHand.moveLeft(RAYMANIZE_DISTANCE);
		rightHand.moveRight(RAYMANIZE_DISTANCE);
		leftLeg.moveDown(RAYMANIZE_DISTANCE / 1.5f);
		rightLeg.moveDown(RAYMANIZE_DISTANCE / 1.5f);
		//tail.moveBack(RAYMANIZE_DISTANCE);
	}


	/**
	 * If waving, wave a little. If walking, walk a little. Adjust the height to the terrain.
	 *
	 * @param gl
	 * @param viewM	View Matrix
	 * @param debug Print debug information?
	 */
	@Override
	public void prepareForDraw(GL3 gl, Mat4 viewM, boolean debug)
	{
		if(waving || wavePhase != 0) wave();
		if(walking || walkPhase != Math.PI / 2)	walk();
		if(rotatingLeft || rotatingRight) rotate();
		/* In case a movement was done by the Model superclass, the bounding box has been invalidated, but the height
		 * not adjusted. Do that now */
		adjustHeightToTerrain(debug);
		super.prepareForDraw(gl, viewM, debug);
	}


	/**
	 * Move the hands a little in waving motion, taking into account where they were last draw.
	 */
	private void wave()
	{
		invalidateBoundingBox();

		/* Because the rotation is relative, first calculate the previous angle */
		float oldAngle = (float)(Math.sin(wavePhase) * waveAmplitude);
		/* The new wave phase is the old phase plus the part of the period that has passed since the last action */
		double oldWavePhase = wavePhase;
		wavePhase = (wavePhase + (System.currentTimeMillis() - lastWaveMovement) / wavePeriod * 2 * Math.PI) % (2 * Math.PI);
		/* Reset the arms if the waving should stop and we're about to pass through the neutral position */
		if((oldWavePhase > wavePhase || oldWavePhase < Math.PI && wavePhase > Math.PI) && !waving)
		{
			wavePhase = 0;
			stoppingWave = false;
		}
		lastWaveMovement = System.currentTimeMillis();
		/* Calculate the new angle belonging with this phase */
		float newAngle = (float)(Math.sin(wavePhase) * waveAmplitude);
		leftHand.rotateCW(newAngle - oldAngle);
		rightHand.rotateCCW(newAngle - oldAngle);
	}


	/**
	 * Move the feet and rest of the cat a little, depending on where they were last draw.
	 */
	private void walk()
	{
		invalidateBoundingBox();

		//All calculations here assume the time passed will not be more than a quarter or half of the period
		/* Find out how much time has passed since the last movement, and save the current time for next time */
		long timePassed = System.currentTimeMillis() - lastWalkMovement;
		lastWalkMovement = System.currentTimeMillis();

		/* Save the old phase and determine the new phase */
		double oldWalkPhase = walkPhase;
		walkPhase = (walkPhase + timePassed / walkPeriod * 2 * Math.PI) % (2 * Math.PI);

		/* Which foot was moving with respect to the ground at the start of this time period? The right leg moves during
		 * the first half of the period; the left during the second */
		Model movingLeg = oldWalkPhase < Math.PI ? rightLeg : leftLeg;
		Model standingLeg = oldWalkPhase < Math.PI ? leftLeg : rightLeg;

		/* If we're meang to stop walking and the legs are about to meet, move only during the previous half phase */
		if(!walking && oldWalkPhase % Math.PI < Math.PI / 2 && walkPhase % Math.PI > Math.PI / 2)
		{
			double fraction = (Math.PI / 2 - oldWalkPhase % Math.PI) / (walkPhase - oldWalkPhase);
			movingLeg.moveForward((float)(walkSpeed * timePassed * fraction));
			standingLeg.moveBack((float)(walkSpeed * timePassed * fraction));
			this.moveForward((float)(walkSpeed * timePassed * fraction));
			walkPhase = Math.PI / 2;
			stoppingWalk = false;
			return;
		}

		/* Move the whole cat */
		this.moveForward((float)(walkSpeed * timePassed));


		/* Find out if the moving leg was switched during this time period */
		if(oldWalkPhase > walkPhase || oldWalkPhase < Math.PI && walkPhase > Math.PI)
		{
			/* This makes things a little more difficult. Find out when during this period that switch took place */
			double oldFraction;
			if(oldWalkPhase > walkPhase)
			{
				oldFraction = (2 * Math.PI - oldWalkPhase) / (2 * Math.PI - oldWalkPhase + walkPhase);
			}
			else
			{
				oldFraction = (Math.PI - oldWalkPhase) / (walkPhase - oldWalkPhase);
			}
			/* Move the legs accordingly. First move one leg forward and the other back during the previous half phase,
			 * then partly reverse this motion during the new phase */
			//movingLeg.moveForward((float)(walkSpeed * timePassed * (oldFraction - (1 - oldFraction)))) simplified to:
			movingLeg.moveForward((float)(walkSpeed * timePassed * (2 * oldFraction - 1)));
			//standingLeg.moveBack((float)(walkSpeed * timePassed * (oldFraction - (1 - oldFraction)))) simplified to:
			standingLeg.moveBack((float)(walkSpeed * timePassed * (2 * oldFraction - 1)));
		}
		else
		{
			movingLeg.moveForward((float)(walkSpeed * timePassed));
			standingLeg.moveBack((float)(walkSpeed * timePassed));
		}
	}


	/**
	 * Start rotating to the left
	 */
	public void startRotatingLeft()
	{
		if(!rotatingLeft)
		{
			rotatingLeft = true;
			lastTurnAction = System.currentTimeMillis();
		}
	}


	/**
	 * Start rotating to the right. If you're already rotating to the left, this will cancel each other out.
	 */
	public void startRotatingRight()
	{
		if(!rotatingRight)
		{
			rotatingRight = true;
			lastTurnAction = System.currentTimeMillis();
		}
	}


	/**
	 * Stop rotating left
	 */
	public void stopRotatingLeft()
	{
		rotatingLeft = false;
	}


	/**
	 * Stop rotating right
	 */
	public void stopRotatingRight()
	{
		rotatingRight = false;
	}


	/**
	 * Rotate the cat model a little, invalidating its bounding box.
	 */
	private void rotate()
	{
		if(rotatingLeft && rotatingRight)
		{
			lastTurnAction = System.currentTimeMillis();
			return;
		}
		if(rotatingLeft) rotateLeft((float)(ROTATION_SPEED * rotationMultiplier * (System.currentTimeMillis() - lastTurnAction)));
		else rotateRight((float)(ROTATION_SPEED * rotationMultiplier * (System.currentTimeMillis() - lastTurnAction)));
		lastTurnAction = System.currentTimeMillis();
	}


	/**
	 * Handle the fact that some keys are no longer pressed
	 *
	 * @param keyCode
	 */
	@Override
	public void handleKeyReleased(int keyCode)
	{
		switch(keyCode)
		{
			case KeyEvent.VK_W:
				stopWalking();
				break;
			case KeyEvent.VK_A:
				rotatingLeft = false;
				break;
			case KeyEvent.VK_D:
				rotatingRight = false;
				break;
		}
	}


	/**
	 * Handle the fact that some keys are down.
	 *
	 * @param pressedKeys
	 * @param ctrlPressed
	 * @param shiftPressed
	 */
	@Override
	public void handleKeyboardInput(Set<KeyboardInput> pressedKeys, boolean ctrlPressed, boolean shiftPressed)
	{
		for(KeyboardInput input : pressedKeys)
		{
			switch(input.getKeyCode())
			{
				case KeyEvent.VK_W:
					if(shiftPressed) startWalking(5, 0.4);
					else startWalking(1, 0.4);
					break;
				case KeyEvent.VK_A:
					if(shiftPressed) rotationMultiplier = 5;
					else rotationMultiplier = 1;
					startRotatingLeft();
					break;
				case KeyEvent.VK_D:
					if(shiftPressed) rotationMultiplier = 5;
					else rotationMultiplier = 1;
					startRotatingRight();
					break;
			}
		}
	}
}
