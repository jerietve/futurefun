package scene;


/**
 * Contains information that can be interpolated over a terrain.
 *
 * @author jeroen
 */
public class TerrainProperties
{
	public float minHeight;
	public float maxHeight;
	public float roughness;

	/* Some predefined TerrainProperties for the terrain editor */
	public static TerrainProperties MOUNTAIN = new TerrainProperties(40, 45, 1.1f);
	public static TerrainProperties LOW_MOUNTAIN = new TerrainProperties(30, 35, 1.1f);
	public static TerrainProperties FLAT_0 = new TerrainProperties(0, 0, 1);
	public static TerrainProperties FLAT_5 = new TerrainProperties(5, 5, 1);
	public static TerrainProperties FLAT_10 = new TerrainProperties(10, 10, 1);
	public static TerrainProperties FLAT_15 = new TerrainProperties(15, 15, 1);
	public static TerrainProperties FLAT_20 = new TerrainProperties(20, 20, 1);
	public static TerrainProperties FLAT_25 = new TerrainProperties(25, 25, 1);
	public static TerrainProperties FLAT_30 = new TerrainProperties(30, 30, 1);
	public static TerrainProperties ALMOST_FLAT_0 = new TerrainProperties(-1, 1, 1);
	public static TerrainProperties ALMOST_FLAT_5 = new TerrainProperties(4, 6, 1);
	public static TerrainProperties ALMOST_FLAT_10 = new TerrainProperties(9, 11, 1);
	public static TerrainProperties ALMOST_FLAT_15 = new TerrainProperties(14, 16, 1);
	public static TerrainProperties ALMOST_FLAT_20 = new TerrainProperties(19, 21, 1);
	public static TerrainProperties ALMOST_FLAT_25 = new TerrainProperties(24, 26, 1);
	public static TerrainProperties ALMOST_FLAT_30 = new TerrainProperties(29, 31, 1);


	/**
	 * Create a new TerrainProperty
	 *
	 * @param minHeight The minimum height at this point
	 * @param maxHeight	The maximum height at this point
	 * @param roughness	The roughness at this point
	 */
	public TerrainProperties(float minHeight, float maxHeight, float roughness)
	{
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;
		this.roughness = roughness;
	}


	/**
	 * Creates a new TerrainProperties object which has all its values interpolated, ds times this one plus (1 - ds)
	 * times the other one.
	 *
	 * @param other
	 * @param ds
	 * @return
	 */
	public TerrainProperties interpolate(TerrainProperties other, float ds)
	{
		float newMinHeight = minHeight * ds + (other.minHeight * (1 - ds));
		float newMaxHeight = maxHeight * ds + (other.maxHeight * (1 - ds));
		float newRoguhness = roughness * ds + (other.roughness * (1 - ds));

		return new TerrainProperties(newMinHeight, newMaxHeight, newRoguhness);
	}
}
