package scene;

import common.shaders.Program;
import java.util.ArrayList;
import javax.media.opengl.GL3;

/**
 * Handles the correct setting of light uniform values in the shaders.
 *
 * @author jeroen
 */
public class LightManager
{
	private ArrayList<Light> lights;
	private int nextIndex;


	/**
	 * Create a new manager.
	 */
	public LightManager()
	{
		lights = new ArrayList<Light>();
		nextIndex = 0;
	}


	/**
	 * Add a light. This light's index will be set to an untaken value.
	 *
	 * @param light
	 */
	public void addLight(Light light)
	{
		light.index = nextIndex++;
		lights.add(light);
	}


	/**
	 * Send the uniform values for each light in this manager to the given shader program.
	 *
	 * @param gl
	 * @param program 
	 */
	public void setUniforms(GL3 gl, Program program)
	{
		for(Light light : lights)
		{
			light.setUniforms(gl, program);
		}
	}
}
