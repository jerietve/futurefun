package scene;

import common.VertexBufferObject;
import common.math.Mat4;
import common.shaders.Program;
import util.Heightmap;

/**
 * A Terrain TreeNode contains a Heightmap, and has Models as children.
 *
 * @author jeroen
 */
public class Terrain extends TreeNode
{
	private Heightmap heightMap;

	public Terrain(Program program, VertexBufferObject vbo, Mat4 modelM, Heightmap heightMap)
	{
		super(program, vbo, modelM);
		this.heightMap = heightMap;
	}


	public Heightmap getHeightmap()
	{
		return heightMap;
	}
}
