package scene;

import common.VertexBufferObject;
import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Point4;
import common.math.Vec4;
import common.shaders.Program;
import java.util.ArrayList;
import javax.media.opengl.GL3;
import util.BoundingBox;

/**
 * The most generic class for the scene graph.
 *
 * @author jeroen
 */
public class TreeNode
{
	protected Mat4 modelMatrix;			// The model matrix for this model in its own frame
	protected boolean modelMatrixChanged;	// Could be called 'descendantTotalModelMatrixnNeedsToBeUpdated', but yeah..
	protected Mat4 totalModelMatrix;	// The model matrix for this model including its parents'
	protected Mat4 normalMatrix;
	protected VertexBufferObject vbo;
	protected TreeNode parent;
	protected ArrayList<TreeNode> children;
	protected Program program;
	private ArrayList<TreeNode[]> rearrangements;

	/**
	 * Construct a TreeNode by providing a VertexBufferObject and this node's model matrix as well as the program that
	 * it's supposed to use.
	 *
	 * @param program	Allowed to be null
	 * @param vbo		Allowed to be null
	 * @param modelM	Should at least be a matrix
	 */
	public TreeNode(Program program, VertexBufferObject vbo, Mat4 modelM)
	{
		this.program = program;
		this.vbo = vbo;
		this.modelMatrix = modelM;
		modelMatrixChanged = true;
		children = new ArrayList<TreeNode>();
	}


	/**
	 * Make this node the root of a tree. It cannot have parents.
	 */
	public void initAsRoot()
	{
		if(parent == null) rearrangements = new ArrayList<TreeNode[]>();
	}


	/**
	 * Draws this TreeNode, then its children. This function will recursively draw the complete tree. It calls
	 * prepareForDraw first for every node.
	 *
	 * @param gl
	 * @param parentModelMatrix
	 */
	public void draw(GL3 gl, Mat4 parentModelMatrix, Mat4 viewM, boolean updateTotalModelMatrix, boolean debug)
	{
		prepareForDraw(gl, viewM, debug);

		/* The totalModelMatrix is the matrix with which to really draw this model, and a base for its children. It
		 * includes the view matrix and all ancestors their model matrix. */
		if(updateTotalModelMatrix || modelMatrixChanged)
		{
			if(parentModelMatrix == null) totalModelMatrix = modelMatrix;
			else totalModelMatrix = parentModelMatrix.mul(modelMatrix);
		}

		if(vbo != null)
		{
			// Draw this node
			vbo.bind(gl);
			program.linkAttribs(gl, vbo.getAttribs());
			program.setUniformMatrix("modelMatrix", totalModelMatrix);
			program.setUniformMatrix("viewMatrix", viewM);
			if(updateTotalModelMatrix || normalMatrix == null) normalMatrix = MatrixMath.transpose(MatrixMath.inverseMat3(parentModelMatrix));
			program.setUniformMatrix("normalMatrix", normalMatrix);
			program.use(gl);
			gl.glDrawArrays(vbo.getFormat(), 0, vbo.getNumVertices());
		}

		for(TreeNode node : children) node.draw(gl, totalModelMatrix, viewM, updateTotalModelMatrix || modelMatrixChanged, debug);
		modelMatrixChanged = false;

		/* When completely done (root), rearrange the tree if necessary */
		if(parent == null) doRearrangements();
	}


	/**
	 * Some nodes may have requested to be moved to another part of the tree. Because it is not allowed to edit a tree
	 * while iterating over it, do this afterward.
	 */
	public void doRearrangements()
	{
		for(TreeNode[] triplet : rearrangements)
		{
			triplet[2].addChild(triplet[0]);
			triplet[1].children.remove(triplet[0]);
		}
		rearrangements.clear();
	}


	/**
	 * Request to be placed somewhere else.
	 *
	 * @param rearrangement TreeNode[3]: {this, moveFrom, moveTo}
	 */
	protected void addRearrangement(TreeNode[] rearrangement)
	{
		rearrangements.add(rearrangement);
	}


	public void prepareForDraw(GL3 gl, Mat4 viewM, boolean debug)
	{
		/* Should be overwritten by classes that want to prepare something */
	}


	/**
	 * Add a child node.
	 *
	 * @param child
	 */
	public void addChild(TreeNode child)
	{
		children.add(child);
		child.parent = this;
	}


	/**
	 * @return The location of the origin of this model in world coordinates
	 */
	public Vec4 getCoordinates()
	{
		return getModelMatrix().mul(new Point4());
	}


	/**
	 * @return All immediate children
	 */
	public ArrayList<TreeNode> getChildren()
	{
		return children;
	}


	/**
	 * @return The root of this tree
	 */
	public TreeNode getRoot()
	{
		if(parent != null) return parent.getRoot();
		else return this;
	}


	/**
	 * @return The model matrix
	 */
	protected Mat4 getModelMatrix()
	{
		if(parent == null) return modelMatrix;
		else return parent.getModelMatrix().mul(modelMatrix);
	}


	/**
	 * Use this to find the first parent that is a Terrain
	 *
	 * @return The first parent Terrain, or null if no such parent exists
	 */
	protected Terrain getTerrainAncestor()
	{
		if(this instanceof Terrain) return (Terrain)this;
		else if(parent != null) return parent.getTerrainAncestor();
		else return null;
	}


	/**
	 * Finds the terrain that this Model is on top of and adds it there. The Model will not be added if it is not at
	 * least partly over any terrain.
	 *
	 * @param model
	 */
	public boolean addModel(Model model)
	{
		BoundingBox box = model.getBoundingBox();
		for(TreeNode terrain : getRoot().children)
		{
			if(((Terrain)terrain).getHeightmap().partlyCovers(box.getxMin(), box.getxMax(), box.getzMin(), box.getzMax()))
			{
				terrain.addChild(model);
				return true;
			}
		}

		return false;
	}
}
