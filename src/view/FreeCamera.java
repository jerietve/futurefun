package view;

import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Vec3;
import common.math.Vec4;
import common.math.VectorMath;
import io.KeyboardInput;
import java.awt.event.KeyEvent;
import java.util.Set;
import util.MathHelper;


/**
 * This Camera can move about freely. Its location can be set directly, and it is moved along the xz-plane. Its
 * orientation is set by two angles. It cannot rotate around its view axis.
 *
 * @author jeroen
 */
public class FreeCamera extends Camera
{
	private Vec4 vrp;		// View reference point
	private float theta;	// The angle between the negative z-axis and n projected on the x-z plane, [0, 2*PI), or
							// the angle to turn around the y-axis
	private float phi;		// The angle between the xz-plane and n, or the angle to turn around u [-PI/2, PI/2]

	/* Movement */
	private float distIncrement, angleIncrement, shiftMultiplier, ctrlMultiplier;

	/**
	 * Creates a new camera centered in the origin looking in the negative z-direction
	 */
	public FreeCamera()
	{
		this(new Vec4(0, 0, 0, 1), 0, 0);
	}


	/**
	 * Create a new FreeCamera
	 *
	 * @param viewReferencePoint	It will be here
	 * @param theta	Rotated left around the y-axis this much
	 * @param phi Then rotated down this much
	 */
	public FreeCamera(Vec4 viewReferencePoint, float theta, float phi)
	{
		vrp = viewReferencePoint;
		camMode = CamMode.FREE;
		distIncrement = 0.05f;
		angleIncrement = MathHelper.deg2rad(0.5f);
		shiftMultiplier = 5;
		ctrlMultiplier = 0.2f;
		this.theta = theta;
		this.phi = phi;
		moved = true;
	}


	/**
	 * Move the camera to the front along the xz-plane.
	 *
	 * @param dist The distance to move
	 */
	public void moveForwardtXZ(float dist)
	{
		vrp.add(MatrixMath.rotationY(theta).mul(VectorMath.mul(Vec4.Z_AXIS_N_NEG, dist)));
		moved = true;
	}


	/**
	 * Move the camera to the rear along the xz-plane.
	 *
	 * @param dist The distance to move
	 */
	public void moveBackXZ(float dist)
	{
		vrp.add(MatrixMath.rotationY(theta).mul(VectorMath.mul(Vec4.Z_AXIS_N, dist)));
		moved = true;
	}


	/**
	 * Move the camera to the left along the xz-plane as seen by someone holding the camera.
	 *
	 * @param dist The distance to move
	 */
	public void moveLeftXZ(float dist)
	{
		vrp.add(u().mulV(dist));
		moved = true;
	}


	/**
	 * Move the camera to the right along the xz-plane as seen by someone holding the camera.
	 *
	 * @param dist The distance to move
	 */
	public void moveRightXZ(float dist)
	{
		moveLeftXZ(-1* dist);
		moved = true;
	}


	/**
	 * Move the camera up (positive y-direction).
	 *
	 * @param dist The distance to move
	 */
	public void moveUp(float dist)
	{
		vrp.add(VectorMath.mul(Vec4.Y_AXIS_N, dist));
		moved = true;
	}


	/**
	 * Move the camera down (negative y-direction).
	 *
	 * @param dist The distance to move
	 */
	public void moveDown(float dist)
	{
		vrp.subV(VectorMath.mul(Vec4.Y_AXIS_N, dist));
		moved = true;
	}


	/**
	 * Turn the camera down in viewing direction (moving the far end down and near end up for a camera man).
	 *
	 * @param angle Angle to turn in degrees
	 */
	public void turnDown(float angle)
	{
		phi = (phi + angle);
		if(phi <= -1 * Math.PI / 2)
		{
			// Make the camera point exactly downward
			phi = (float)(-1 * Math.PI / 2);
		}
		else if(phi >= Math.PI / 2)
		{
			// Make the camera point exactly upwarad
			phi = (float)(Math.PI / 2);
		}
		moved = true;
	}


	/**
	 * Turn the camera up in viewing direction (moving the far end up and near end down for a camera man).
	 *
	 * @param angle Angle to turn in degrees
	 */
	public void turnUp(float angle)
	{
		turnDown(angle * -1);
		moved = true;
	}


	/**
	 * Turn the camera left around the y-axis.
	 *
	 * @param angle Angle to turn in degrees
	 */
	public void turnLeft(float angle)
	{
		theta = (theta + angle) % (float)(Math.PI * 2);
		moved = true;
	}


	/**
	 * Turn the camera right in the camera frame.
	 *
	 * @param angle Angle to turn in degrees
	 */
	public void turnRight(float angle)
	{
		turnLeft(angle * -1);
		moved = true;
	}


	private Vec4 u()
	{
		return MatrixMath.rotationY(theta).mul(Vec4.X_AXIS_N_NEG);
	}


	@Override
	public Mat4 getViewMatrix()
	{
		if(!moved) return viewMatrix;
		moved = false;
		/* Calculate the translation matrix. Reverse the sign because we're moving the objects relative to the camera. */
		Mat4 T = MatrixMath.translate(-vrp.get(0), -vrp.get(1), -vrp.get(2));
		/* Calculate the rotation matrix. Because the objects are rotated and moved relative to the camera instead, we
		 * must be a little careful here. Objects are translated and rotated in the world frame. This means they must be
		 * rotated first and then translated.
		 * The camera is rotated around the y-axis first, and then around u. The angles must be inverted, just like the
		 * translations.
		 */
		Mat4 R = MatrixMath.rotationY(-theta).mul(MatrixMath.rotate(-phi, u()));
		viewMatrix = R.mul(T);
		return viewMatrix;
	}


	@Override
	public Vec3 getPosition()
	{
		return new Vec3(vrp);
	}


	@Override
	public FreeCamera detach()
	{
		return this;
	}


	@Override
	public void handleKeyboardInput(Set<KeyboardInput> pressedKeys, boolean ctrlPressed, boolean shiftPressed)
	{
		for(KeyboardInput input : pressedKeys)
		{
			switch(input.getKeyCode())
			{
				case KeyEvent.VK_W:
					// Move the camera delta in viewing direction along the xz-plane
					moveForwardtXZ(shiftPressed ? distIncrement * shiftMultiplier : ctrlPressed ? distIncrement * ctrlMultiplier : distIncrement);
					break;
				case KeyEvent.VK_S:
					// Move the camera delta in negative viewing direction along the xz-plane
					moveBackXZ(shiftPressed ? distIncrement * shiftMultiplier : ctrlPressed ? distIncrement * ctrlMultiplier : distIncrement);
					break;
				case KeyEvent.VK_A:
					// Move the camera delta to the left
					moveLeftXZ(shiftPressed ? distIncrement * shiftMultiplier : ctrlPressed ? distIncrement * ctrlMultiplier : distIncrement);
					break;
				case KeyEvent.VK_D:
					// Move the camera delta to the right
					moveRightXZ(shiftPressed ? distIncrement * shiftMultiplier : ctrlPressed ? distIncrement * ctrlMultiplier : distIncrement);
					break;
				case KeyEvent.VK_F1:
					// Move the camera delta down in Y-direction
					moveDown(shiftPressed ? distIncrement * shiftMultiplier : ctrlPressed ? distIncrement * ctrlMultiplier : distIncrement);
					break;
				case KeyEvent.VK_F4:
					// Move the camera delta up in Y-direction
					moveUp(shiftPressed ? distIncrement * shiftMultiplier : ctrlPressed ? distIncrement * ctrlMultiplier : distIncrement);
					break;
				case KeyEvent.VK_UP:
					// Turn the camera down in viewing direction
					turnDown(shiftPressed ? angleIncrement * shiftMultiplier : ctrlPressed ? angleIncrement * ctrlMultiplier : angleIncrement);
					break;
				case KeyEvent.VK_DOWN:
					// Turn the camera up in viewing direction
					turnUp(shiftPressed ? angleIncrement * shiftMultiplier : ctrlPressed ? angleIncrement * ctrlMultiplier : angleIncrement);
					break;
				case KeyEvent.VK_LEFT:
					// Turn the camera left in viewing direction
					turnLeft(shiftPressed ? angleIncrement * shiftMultiplier : ctrlPressed ? angleIncrement * ctrlMultiplier : angleIncrement);
					break;
				case KeyEvent.VK_RIGHT:
					// Turn the camera right in viewing direction
					turnRight(shiftPressed ? angleIncrement * shiftMultiplier : ctrlPressed ? angleIncrement * ctrlMultiplier : angleIncrement);
					break;
			}
		}
	}


	@Override
	public String toString()
	{
		return "vrp: " + vrp.toString();
	}
}
