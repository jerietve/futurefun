package view;

import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Vec3;
import common.math.Vec4;
import common.math.VectorMath;
import io.KeyboardInput;
import java.util.Set;
import scene.Model;

/**
 *
 * @author jeroen
 */
public class ThirdPersonCamera extends Camera
{
	private Model attache;

	public ThirdPersonCamera(Model attache)
	{
		this.attache = attache;
		camMode = CamMode.THIRD_PERSON;
	}


	/**
	 * @return The Model this camera is attached to
	 */
	public Model getAttache()
	{
		return attache;
	}


	@Override
	public void handleKeyboardInput(Set<KeyboardInput> pressedKeys, boolean ctrlPressed, boolean shiftPressed)
	{
		throw new UnsupportedOperationException("Not supported yet.");
	}


	@Override
	public FreeCamera detach()
	{
		/* Get the location of the camera */
		Vec4 viewReferencePoint = MatrixMath.translate(attache.getForwardDirection().mulV(-2.5f)).mul(MatrixMath.translate(attache.getUpDirection().mulV(0.5f))).mul(attache.getCoordinates());
		/* Calculate the viewing direction */
		Vec4 viewDirection = attache.getCoordinates().subV(viewReferencePoint);
		/* Project the view direction on the xz-plane */
		Vec4 viewDirProjected = VectorMath.projectPlane(viewDirection, Vec4.Y_AXIS_N);
		/* Calculate the angles between the projected view dir and the positive x and z-axes */
		double a = VectorMath.directionCosineA(viewDirProjected);
		double c = VectorMath.directionCosineC(viewDirProjected);

		/* Use these angles (which run [0, PI]) to calculate theta (which runs [0, 2*PI)) */
		double theta;
		double halfPi = Math.PI / 2;
		if(a < halfPi && c > halfPi) theta = halfPi - a;		// Quadrant I
		if(a > halfPi && c > halfPi) theta = a - halfPi;		// Quadrant II
		else if(a > halfPi && c < halfPi) theta = Math.PI - c;	// Quadrant III
		else theta = Math.PI + c;								// Quadrant IV
		float phi = (float)((VectorMath.directionCosineB(viewDirection) - halfPi) % (2 * Math.PI));
		return new FreeCamera(viewReferencePoint, (float)theta, phi);
	}


	@Override
	public Mat4 getViewMatrix()
	{
		return MatrixMath.lookAt(MatrixMath.translate(attache.getForwardDirection().mulV(-2.5f)).mul(MatrixMath.translate(attache.getUpDirection().mulV(0.5f))).mul(attache.getCoordinates()), attache.getCoordinates(), Vec4.Y_AXIS_N);
	}


	@Override
	public Vec3 getPosition()
	{
		return new Vec3(MatrixMath.translate(attache.getForwardDirection().mulV(-2.5f)).mul(MatrixMath.translate(attache.getUpDirection().mulV(0.5f))).mul(attache.getCoordinates()));
	}


	@Override
	public String toString()
	{
		return "Third person camera";
	}
}
