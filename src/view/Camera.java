package view;

import common.math.Mat4;
import common.math.Vec3;
import io.KeyboardInput;
import java.util.Set;
import scene.Model;

/**
 * Generic camera class that has to be extended by any camera.
 *
 * @author jeroen
 */
abstract public class Camera
{
	public enum CamMode {THIRD_PERSON, FREE};

	protected CamMode camMode;
	protected Mat4 viewMatrix;
	protected boolean moved;	// Moved since last view matrix calculation


	/**
	 * Turn the camera into a third person camera focused on the model
	 *
	 * @param model
	 * @return
	 */
	public ThirdPersonCamera attachThirdPerson(Model model)
	{
		return new ThirdPersonCamera(model);
	}


	/**
	 * Turn the camera into a free camera
	 */
	abstract public FreeCamera detach();


	/**
	 * Returns the view matrix. Applying this matrix to all vertices in the pipeline should place them correctly with
	 * respect to this camera.
	 *
	 * @return
	 */
	abstract public Mat4 getViewMatrix();
	abstract public Vec3 getPosition();
	abstract public void handleKeyboardInput(Set<KeyboardInput> pressedKeys, boolean ctrlPressed, boolean shiftPressed);


	/**
	 * @return True if this camera is a FreeCamera
	 */
	public boolean isFree()
	{
		return camMode == CamMode.FREE;
	}


	/**
	 * @return true if this camera is a third person camera
	 */
	public boolean isThirdPerson()
	{
		return camMode == CamMode.THIRD_PERSON;
	}

	@Override
	abstract public String toString();
}
