import com.jogamp.common.nio.Buffers;
import com.jogamp.opengl.util.FPSAnimator;
import common.math.Mat4;
import common.math.MatrixMath;
import common.math.Vec2;
import common.math.Vec4;
import common.shaders.FragmentShader;
import common.shaders.Program;
import common.shaders.ProgramLoader;
import common.shaders.VertexShader;
import exceptions.CompilationFailedException;
import io.KeyboardInput;
import io.SGFLoader;
import io.Texture;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GL3;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import scene.Cat;
import scene.F16;
import scene.Light;
import scene.LightManager;
import scene.Model;
import scene.ModelBuilder;
import scene.Terrain;
import scene.TreeNode;
import util.Heightmap;
import util.MathHelper;
import util.TerrainGenerator;
import view.Camera;
import view.ThirdPersonCamera;


class Project implements GLEventListener, KeyListener
{
	/* SETTINGS FOR GRADER */
	private final boolean DAY_NIGHT_CYCLE_ON = false;
	private final float MINUTES_PER_DAY = 1;
	private final float SNOW_LINE_BOTTOM_WARM = 25;
	private final float SNOW_LINE_TOP_WARM = 28;
	private final float SNOW_LINE_BOTTOM_COLD = 10;
	private final float SNOW_LINE_TOP_COLD = 20;
	private final String TERRAIN_TO_LOAD = "testterrain.txt";
	private final Vec2 TERRAIN_UPPER_LEFT_POINT = new Vec2(-100, -100);
	private final Vec2 TERRAIN_LOWER_RIGHT_POINT = new Vec2(100, 100);
	private final int SUB_TERRAIN_SIZE_POWER_OF_TWO = 16;
	/* END OF SETTINGS FOR GRADER */

	/* The shaders and AWT related variables */
	private Frame frame;
	private ProgramLoader loader;
	private Program terrainProgram;
	private Program modelProgram;
	private Program lightProgram;
	private int frameWidth = 800, frameHeight = 600;

	/* Scene */
	private Cat cat;
	private F16 f16;
	private TreeNode root;
	private TreeNode lightTree;
	private ArrayList<Model> models;
	private LightManager lightManager;
	private Light sun;

	/* Viewing */
	private Camera cam;
	private Mat4 projectionM;
	private Mat4 viewM;

	/* Input */
	private final Set<KeyboardInput> pressedKeys = new HashSet<KeyboardInput>();
	private boolean ctrlPressed = false;
	private boolean shiftPressed = false;

	/* Debugging */
	private boolean debugOnce = false;

	/**
	 * Constructor for our empty program, this sets up the window (JFrame), creates a GLCanvas and starts the Animator
	 */
	public Project(GLCapabilities capabilities)
	{
		this.loader = new ProgramLoader();

		// Initialize the GL component and the animator
		final GLCanvas glComponent = new GLCanvas(capabilities);
		glComponent.addGLEventListener(this);
		glComponent.addKeyListener(this);

		// Create the main frame
		this.frame = new JFrame("FutureFun - Computer Graphics Final Project");
		this.frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				Project.this.frame.dispose();
				System.exit(0);
			}
		});
		this.frame.setLayout(new BorderLayout());
		glComponent.setPreferredSize(new Dimension(frameWidth, frameHeight));
		this.frame.add(glComponent, BorderLayout.CENTER);
		this.frame.pack();
		this.frame.setVisible(true);

		//Make sure we have focus inside the window, so keyboard interaction works from frame 1
		glComponent.setFocusable(true);
		glComponent.requestFocus();
	}


	/**
	 * display() will be called repeatedly by the Animator when Animator is done it will swap buffers and update
	 * display. In this function you can specify what will appear on the screen. glClear() clears the color buffer using
	 * the clear color specified using glClearColor()
	 */
	@Override
	public void display(GLAutoDrawable drawable)
	{
		final GL3 gl = drawable.getGL().getGL3();
		handlePressedKeys(gl);

		gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);

		if(DAY_NIGHT_CYCLE_ON)
		{
			sun.setColor(getSunColor());
			float[] snowLine = getSnowLine();
			terrainProgram.setUniform("snowLineBottom", snowLine[0]);
			terrainProgram.setUniform("snowLineTop", snowLine[1]);
			adjustSky(gl);
			sun.setUniforms(gl, modelProgram);
			sun.setUniforms(gl, terrainProgram);
		}

		viewM = cam.getViewMatrix();
		loader.setUniformMatrix("viewMatrix", viewM);
		loader.setUniformVector("cameraPosition", cam.getPosition());

		root.draw(gl, null, viewM, false, debugOnce);
		lightTree.draw(gl, null, viewM, false, debugOnce);
		System.gc();
		debugOnce = false;
	}


	/**
	 * Init() will be called when your program starts and is typically used for configuring which buffers should be used
	 * in the OpenGL pipeline. The gl.glClearColor() is already in there to set the clear color to white.
	 */
	@Override
	public void init(GLAutoDrawable drawable)
	{
		final GL3 gl = drawable.getGL().getGL3();
		loadPrograms(gl);

		/* Start the scene graph */
		root = new TreeNode(null, null, new Mat4());
		root.initAsRoot();

		/* Load a terrain */
		loadTerrain(gl);

		/* Load the models */
		loadModels(gl);

		/* Position the camera */
		cam = new ThirdPersonCamera(cat);
		recalculateProjectionM(frameWidth, frameHeight);
		loader.setUniformMatrix("projectionMatrix", projectionM);

		/* Put some light in the scene */
		loadLights(gl);

		// Create and start the animator
		final FPSAnimator animator = new FPSAnimator(drawable, 60);
		animator.start();

		gl.glEnable(GL.GL_DEPTH_TEST);
		gl.glClearColor(0.14f, 0.9f, 0.91f, 1f);
	}


	/**
	 * reshape() specifies what happens when the user reshapes the window
	 */
	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int w, int h)
	{
		final GL3 gl = drawable.getGL().getGL3();
		gl.glViewport(0, 0, w, h);
		this.frameWidth = w;
		this.frameHeight = h;
		recalculateProjectionM(w, h);
		loader.setUniformMatrix("projectionMatrix", projectionM);
	}


	/**
	 * displayChanged() specifies what happens when the display mode or the display device is changed
	 */
	public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged)
	{
	}


	/**
	 * dispose() will run when the window is closed
	 */
	@Override
	public void dispose(GLAutoDrawable drawable)
	{
		final GL3 gl = drawable.getGL().getGL3();
		this.loader.cleanup(gl);
	}


	/**
	 * Your program starts here
	 */
	public static void main(String[] args)
	{
		// setting this true causes window events not to get sent on Linux if you run from inside Eclipse
		// GLProfile.initSingleton(true);

		//A OpenGL 3 profile is retrieved and used to set up a new window.
		final GLProfile profile = GLProfile.get(GLProfile.GL3);
		final GLCapabilities capabilities = new GLCapabilities(profile);

		//We invokeLater to ensure that the gl thread is invoked by the thread that handles display stuff.
		SwingUtilities.invokeLater(new Runnable()
		{
			@Override
			public void run()
			{
				new Project(capabilities);
			}
		});
	}


	@Override
	public void keyTyped(KeyEvent e)
	{
	}


	@Override
	public void keyPressed(KeyEvent e)
	{
		pressedKeys.add(new KeyboardInput(e.getKeyCode()));
	}


	/**
	 * The release of a key will be made known to the models and camera receiving keyboard input at the moment, and the
	 * key will be removed from the pressed keys list
	 *
	 * @param e Contains information on the released key
	 */
	@Override
	public void keyReleased(KeyEvent e)
	{
		switch(e.getKeyCode())
		{
			case KeyEvent.VK_CONTROL:
				ctrlPressed = false;
				break;
			case KeyEvent.VK_SHIFT:
				shiftPressed = false;
				break;
			case KeyEvent.VK_COMMA:
				cat.stopRotatingLeft();
				break;
			case KeyEvent.VK_PERIOD:
				cat.stopRotatingRight();
				break;
			case KeyEvent.VK_W:
			case KeyEvent.VK_A:
			case KeyEvent.VK_D:
				if(cam.isThirdPerson()) ((ThirdPersonCamera)cam).getAttache().handleKeyReleased(e.getKeyCode());
				break;
		}
		pressedKeys.remove(new KeyboardInput(e.getKeyCode()));
	}


	/**
	 * Checks the list of keys that are down at the moment, and handles them.
	 *
	 * @param e Contains information on the released key
	 */
	private void handlePressedKeys(GL3 gl)
	{
		for(KeyboardInput input : pressedKeys)
		{
			switch(input.getKeyCode())
			{
				case KeyEvent.VK_CONTROL:
					ctrlPressed = true;
					break;
				case KeyEvent.VK_SHIFT:
					shiftPressed = true;
					break;
				case KeyEvent.VK_W:
				case KeyEvent.VK_S:
				case KeyEvent.VK_A:
				case KeyEvent.VK_D:
					if(cam.isFree()) cam.handleKeyboardInput(pressedKeys, ctrlPressed, shiftPressed);
					else if(cam.isThirdPerson()) ((ThirdPersonCamera)cam).getAttache().handleKeyboardInput(pressedKeys, ctrlPressed, shiftPressed);
					break;
				case KeyEvent.VK_F1:
				case KeyEvent.VK_F4:
				case KeyEvent.VK_UP:
				case KeyEvent.VK_DOWN:
				case KeyEvent.VK_LEFT:
				case KeyEvent.VK_RIGHT:
					if(cam.isFree()) cam.handleKeyboardInput(pressedKeys, ctrlPressed, shiftPressed);
					break;
				case KeyEvent.VK_T:
					// Switch rasterization mode
					if(!input.itsBeen(200)) break;
					IntBuffer currentMode = Buffers.newDirectIntBuffer(1);
					gl.glGetIntegerv(GL2.GL_POLYGON_MODE, currentMode);
					if(currentMode.get(0) == GL3.GL_LINE) gl.glPolygonMode(GL3.GL_FRONT_AND_BACK, GL3.GL_FILL);
					else gl.glPolygonMode(GL3.GL_FRONT_AND_BACK, GL3.GL_LINE);
					input.actionTaken();
					break;
				case KeyEvent.VK_C:
					if(input.itsBeen(500))
					{
						if(cam.isFree()) cam = cam.attachThirdPerson(cat);
						else cam = cam.detach();
						input.actionTaken();
					}
					break;
				case KeyEvent.VK_ESCAPE:
					// Exit the game
					System.exit(0);
					break;
				case KeyEvent.VK_1:
					if(!input.itsBeen(500)) break;
					cat.toggleWave();
					input.actionTaken();
					break;
				case KeyEvent.VK_2:
					if(!input.itsBeen(500)) break;
					cat.toggleWalk();
					input.actionTaken();
					break;
				case KeyEvent.VK_COMMA:
					cat.startRotatingLeft();
					break;
				case KeyEvent.VK_PERIOD:
					cat.startRotatingRight();
					break;
				case KeyEvent.VK_3:
					if(input.itsBeen(500))
					{
						System.out.println(cam.toString());
						input.actionTaken();
					}
				case KeyEvent.VK_4:
					if(input.itsBeen(500))
					{
						for(Model model : models)
						{
							model.toggleBoundingBoxes();
						}
						input.actionTaken();
					}
					break;
				case KeyEvent.VK_R:
					debugOnce = true;
					break;
			}
		}
	}


	/**
	 * Recalculate the projection matrix
	 *
	 * @param w	Width of the JFrame in pixels
	 * @param h	Height of the JFrame in pixels
	 */
	private void recalculateProjectionM(int w, int h)
	{
		if(h <= w)
		{
			projectionM = MatrixMath.perspective(MathHelper.deg2rad(45), (float) w / (float) h, 0.1f, 250f);
		}
		else
		{
			projectionM = MatrixMath.perspective(MathHelper.deg2rad(45), (float) h / (float) w, 0.1f, 250f);
		}
	}


	/**
	 * Compiles, attaches, links, creates all shaders and programs used.
	 *
	 * @param gl
	 */
	private void loadPrograms(final GL3 gl)
	{
		//Load and compile shaders, then use program.
		try
		{
			VertexShader terrainVS = new VertexShader("shaders/terrain.vs.glsl");
			terrainVS.init(gl);
			FragmentShader terrainFS = new FragmentShader("shaders/terrain.fs.glsl");
			terrainFS.init(gl);
			FragmentShader lightingFS = new FragmentShader("shaders/lighting.fs.glsl");
			lightingFS.init(gl);
			terrainProgram = loader.createProgram(gl, terrainFS, terrainVS, lightingFS);
			VertexShader modelVS = new VertexShader("shaders/models.vs.glsl");
			modelVS.init(gl);
			FragmentShader modelFS = new FragmentShader("shaders/models.fs.glsl");
			modelFS.init(gl);
			modelProgram = loader.createProgram(gl, modelVS, modelFS, lightingFS);
			lightProgram = loader.createProgram(gl, "shaders/light.vs.glsl", "shaders/light.fs.glsl");
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(CompilationFailedException e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Loads a terrain (currently from a file, but terrains can be generated in three different ways, see
	 * TerrainGenerator). The terrain is added to the scene graph and its properties are sent to the GPU.
	 *
	 * @param gl
	 */
	private void loadTerrain(final GL3 gl)
	{
		try
		{
			Terrain[][] terrains = TerrainGenerator.generateTerrainArray(gl, terrainProgram, "terrains/" + TERRAIN_TO_LOAD,
					new Mat4(), TERRAIN_UPPER_LEFT_POINT, TERRAIN_LOWER_RIGHT_POINT, SUB_TERRAIN_SIZE_POWER_OF_TWO, 10, 10);
			for(int j = 0; j < terrains[0].length; j++)
			{
				for(int i = 0; i < terrains.length; i++)
				{
					Terrain t = terrains[i][j];
					if(t != null)
					{
						root.addChild(t);
					}
				}
			}
		}
		catch(IOException ex)
		{
			java.util.logging.Logger.getLogger(Project.class.getName()).log(Level.SEVERE, null, ex);
		}
		float[] snowLine = getSnowLine();
		terrainProgram.setUniform("snowLineBottom", snowLine[0]);
		terrainProgram.setUniform("snowLineTop", snowLine[1]);

		Texture grass = new Texture("textures/Grass0101_2_S.jpg", GL3.GL_TEXTURE0);
		grass.init(gl);
		grass.bind(gl, terrainProgram, "texture2");
		Texture frozenGround = new Texture("textures/GroundFrozen0033_2_S.jpg", GL3.GL_TEXTURE1);
		frozenGround.init(gl);
		frozenGround.bind(gl, terrainProgram, "texture2");
	}


	/**
	 * Get the current height of the bottom and top of the snow line
	 *
	 * @return float[2], with the bottom at [0] and the top at [1]
	 */
	private float[] getSnowLine()
	{
		float[] snowLine = new float[2];
		if(DAY_NIGHT_CYCLE_ON)
		{
			float percentOfDay = MathHelper.percentageOfDay(MINUTES_PER_DAY);
			if(percentOfDay > 0.25 && percentOfDay < 0.75)
			{
				/* Snow melts during the day */
				snowLine[0] = SNOW_LINE_BOTTOM_COLD + (SNOW_LINE_BOTTOM_WARM - SNOW_LINE_BOTTOM_COLD) * 2 * (percentOfDay - 0.25f);
				snowLine[1] = SNOW_LINE_TOP_COLD + (SNOW_LINE_TOP_WARM - SNOW_LINE_TOP_COLD) * 2 * (percentOfDay - 0.25f);
			}
			else
			{
				snowLine[0] = SNOW_LINE_BOTTOM_WARM - (SNOW_LINE_BOTTOM_WARM - SNOW_LINE_BOTTOM_COLD) * 2 * ((percentOfDay + 0.25f) % 1);
				snowLine[1] = SNOW_LINE_TOP_WARM - (SNOW_LINE_TOP_WARM - SNOW_LINE_TOP_COLD) * 2 * ((percentOfDay + 0.25f) % 1);
			}
		}
		else
		{
			snowLine[0] = 25;
			snowLine[1] = 28;
		}
		return snowLine;
	}


	/**
	 * Loads all models from their files, adds them to the model list and the scene graph and puts them in place
	 *
	 * @param gl
	 */
	private void loadModels(final GL3 gl)
	{
		models = new ArrayList<Model>();
		try
		{
			/* Create */
			// Model textures were hacked in at the last moment. Just throw the texture for the cat into the shader
			Texture fur = new Texture("textures/orange_fur.jpg", GL3.GL_TEXTURE2);
			fur.init(gl);
			fur.bind(gl, modelProgram, "texture1");
			cat = ModelBuilder.buildCat(SGFLoader.LoadModel(gl, modelProgram, "models/cat"), modelProgram, fur);
			f16 = ModelBuilder.buildF16(SGFLoader.LoadModel(gl, modelProgram, "models/f-16"), modelProgram);
			/* Add */
			root.addModel(cat);
			root.addModel(f16);
			models.add(cat);
			models.add(f16);
			/* Adjust */
			f16.moveRight(2);
			f16.moveForward(2);
			f16.adjustHeightToTerrain(false);
			cat.moveForward(0);
			cat.adjustHeightToTerrain(false);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}


	/**
	 * Loads all lights (currently the sun and a red light) and sends their information, as well as the material
	 * properties for the terrain and models related to light, to the programs.
	 *
	 * @param gl
	 */
	private void loadLights(final GL3 gl)
	{
		lightManager = new LightManager();
		Vec4 sunColor = getSunColor();
		sun = new Light(new Vec4(-1, 1, -1, 0), sunColor, sunColor, sunColor);
		lightManager.addLight(sun);
		/* Add a red light left in front of the cat, one meter above the surface there */
		float height = 0;
		for(TreeNode terrain : root.getChildren())
		{
			Heightmap map = ((Terrain)terrain).getHeightmap();
			if(map.isWithinHeightmap(-1, -1)) height = map.getHeight(-1, -1, false) + 1;
		}
		Light redLight = new Light(new Vec4(-1, height, -1, 1), new Vec4(0.5f, 0f, 0f, 1.0f), new Vec4(0.9f, 0.0f, 0.0f, 1.0f), new Vec4(1.0f, 0.0f, 0.0f, 1.0f));
		lightManager.addLight(redLight);

		lightManager.setUniforms(gl, modelProgram);
		lightManager.setUniforms(gl, terrainProgram);
		lightTree = new TreeNode(lightProgram, null, new Mat4());
		lightTree.initAsRoot();
		lightTree.addChild(new TreeNode(lightProgram, redLight.getVBO(gl), redLight.getModelMatrix()));
		modelProgram.setUniformVector("materialAmbient", new Vec4(0.9f, 0.9f, 0.9f, 1.0f));
		modelProgram.setUniformVector("materialDiffuse", new Vec4(0.9f, 0.9f, 0.9f, 1.0f));
		modelProgram.setUniformVector("materialSpecular", new Vec4(1.0f, 1.0f, 1.0f, 1.0f));
		modelProgram.setUniform("materialShininess", 100f);
		terrainProgram.setUniformVector("materialAmbient", new Vec4(0.9f, 0.9f, 0.9f, 1.0f));
		terrainProgram.setUniformVector("materialDiffuse", new Vec4(0.9f, 0.9f, 0.9f, 1.0f));
		terrainProgram.setUniformVector("materialSpecular", new Vec4(1.0f, 1.0f, 1.0f, 1.0f));
		terrainProgram.setUniform("materialShininess", 10f);
	}


	/**
	 * @return The current color of the sun. It's off at night, and brightest at noon.
	 */
	private Vec4 getSunColor()
	{
		if(DAY_NIGHT_CYCLE_ON)
		{
			float sunStrength;
			float percentOfDay = MathHelper.percentageOfDay(MINUTES_PER_DAY);
			if(percentOfDay < 0.25 || percentOfDay > 0.75)
			{
				/* Sun rises at 6, sets at 18 */
				sunStrength = 0;
			}
			else
			{
				/* Sunlight goes in strength from 0.0 to 0.8 at noon */
				sunStrength = 0.8f - (float)Math.abs(0.5 - percentOfDay) * 3.2f;
			}
			return new Vec4(sunStrength, sunStrength, sunStrength, 1);
		}
		else return new Vec4(.8f, .8f, .8f, 1);
	}


	/**
	 * Sets the sky color dependent on the time of day.
	 *
	 * @param gl
	 */
	private void adjustSky(GL3 gl)
	{
		float greenBlue = .9f - (float)Math.abs(.5 - MathHelper.percentageOfDay(MINUTES_PER_DAY)) * .9f * 2;
		gl.glClearColor(.14f, greenBlue, greenBlue, 1);
	}
}