#version 410

uniform sampler2D texture1;
uniform sampler2D texture2;
uniform float snowLineBottom;
uniform float snowLineTop;

in VertexData
{
	vec2 texCoord;
	float height;
	vec3 N;
	vec3[2] L;
	vec3 E;
};

out vec4 fragColor;

void calculateLighting(vec3, vec3[2], vec3, out vec4, out vec4, out vec4);

void main()
{
	vec4 ambient, diffuse, specular;
	calculateLighting(N, L, E, ambient, diffuse, specular);

	/* Calculate the relative visibility of the textures */
	float texture1Strength, texture2Strength;
	if(height < snowLineBottom) texture2Strength = 0;
	else if(height < snowLineTop) texture2Strength = (height - snowLineBottom) / (snowLineTop - snowLineBottom);
	else texture2Strength = 1;
	texture1Strength = 1 - texture2Strength;

    fragColor = (texture1Strength * texture(texture1, texCoord) + texture2Strength * texture(texture2, texCoord)) * (ambient + diffuse + specular);
}
