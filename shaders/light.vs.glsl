#version 410

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;

in vec4 vPosition;
in vec4 vColor;

out vec4 lightColor;

void main()
{
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vPosition;
	lightColor = vColor;
}