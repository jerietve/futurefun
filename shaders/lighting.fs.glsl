#version 410

struct LightProperties
{
	vec4 lightPosition;
	vec4 lightAmbient;
	vec4 lightDiffuse;
	vec4 lightSpecular;
};

uniform vec4 materialAmbient;
uniform vec4 materialDiffuse;
uniform vec4 materialSpecular;
uniform float materialShininess;
/* This block is taken by uniform block indices 0 through lightPropertiesBlocks.length - 1 */
layout (std140) uniform LightPropertiesBlock
{
	uniform LightProperties lightProperties;
} lightPropertiesBlocks[2];

vec4 calculateSpecular(vec3 N, vec3 H, float Kd, int index)
{
	if(Kd == 0.0 )
	{
		/* Light is behind the surface */
		return vec4(0.0, 0.0, 0.0, 1.0);
	}
	else
	{
		float s = dot(N, H);
		if(s > 0.0) return pow(s, materialShininess) * lightPropertiesBlocks[index].lightProperties.lightSpecular * materialSpecular;
		else return vec4(0.0, 0.0, 0.0, 1.0);
	}
}


void calculateLighting(vec3 N, vec3[2] L, vec3 E, out vec4 ambient, out vec4 diffuse, out vec4 specular)
{
	/* Initialize to black. There are two kinds of lights: directional (sun) and the rest. */
	vec4 directionalAmbient = vec4(0, 0, 0, 1);
	vec4 directionalDiffuse = vec4(0, 0, 0, 1);
	vec4 directionalSpecular = vec4(0, 0, 0, 1);
	vec4 otherAmbient = vec4(0, 0, 0, 1);
	vec4 otherDiffuse = vec4(0, 0, 0, 1);
	vec4 otherSpecular = vec4(0, 0, 0, 1);
	int directionalLights = 0;
	int otherLights = 0;

	/* Calculate the values for each light and add everything together */
	vec3 NN = normalize(N);
	vec3 EE = normalize(E);
	for(int i = 0; i < L.length(); i++)
	{
		vec3 LL = normalize(L[i]);
		vec3 H = normalize(LL + EE);

		if(lightPropertiesBlocks[i].lightProperties.lightPosition.w == 0.0)
		{
			directionalLights++;
			directionalAmbient += lightPropertiesBlocks[i].lightProperties.lightAmbient * materialAmbient;
			float Kd = max(dot(LL, NN), 0.0);
			directionalDiffuse += Kd * lightPropertiesBlocks[i].lightProperties.lightDiffuse * materialDiffuse;
			directionalSpecular += calculateSpecular(NN, H, Kd, i);
		}
		else
		{
			otherLights++;
			otherAmbient += lightPropertiesBlocks[i].lightProperties.lightAmbient * materialAmbient;
			float Kd = max(dot(LL, NN), 0.0);
			otherDiffuse += Kd * lightPropertiesBlocks[i].lightProperties.lightDiffuse * materialDiffuse;
			otherSpecular += calculateSpecular(NN, H, Kd, i);
		}
	}

	/* Average the values */
	directionalAmbient /= directionalLights;
	directionalDiffuse /= directionalLights;
	directionalSpecular /= directionalLights;
	otherAmbient /= otherLights;
	otherDiffuse /= otherLights;
	otherSpecular /= otherLights;

	/* Combine the different kinds of lights */
	ambient = directionalAmbient + (vec4(1, 1, 1, 1) - directionalAmbient) * otherAmbient;
	diffuse = directionalDiffuse + (vec4(1, 1, 1, 1) - directionalDiffuse) * otherDiffuse;
	specular = directionalSpecular + (vec4(1, 1, 1, 1) - directionalSpecular) * otherSpecular;
}