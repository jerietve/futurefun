#version 410

struct LightProperties
{
	vec4 lightPosition;
	vec4 lightAmbient;
	vec4 lightDiffuse;
	vec4 lightSpecular;
};

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 normalMatrix;
uniform vec3 cameraPosition;
uniform int isBoundingBox;
/* This block is taken by uniform block indices 0 through lightPropertiesBlocks.length - 1 */
layout (std140) uniform LightPropertiesBlock
{
	uniform LightProperties lightProperties;
} lightPropertiesBlocks[2];

in vec4 vPosition;
in vec4 vColor;
in vec4 vNormal;

out VertexData
{
	vec4 color;
	vec3 N;
	vec3[2] L;
	vec3 E;
	vec2 texCoord;
} VertexOut;

void main()
{
	mat4 modelViewM = viewMatrix * modelMatrix;
	gl_Position = projectionMatrix * modelViewM * vPosition;
	
	if(isBoundingBox == 1)
	{
		VertexOut.color = vColor;
		return;		
	}
	
	/* Calculate the normal vector in world coordinates */
	VertexOut.N = (normalMatrix * vNormal).xyz;
	
	/* Calculate the light vector in world coordinates */
	for(int i = 0; i < 2; i++)
	{
		if(lightPropertiesBlocks[i].lightProperties.lightPosition.w == 0.0)
		{
			VertexOut.L[i] = lightPropertiesBlocks[i].lightProperties.lightPosition.xyz;
		}
		else
		{
			VertexOut.L[i] = lightPropertiesBlocks[i].lightProperties.lightPosition.xyz - (modelMatrix * vPosition).xyz;
		}
	}
	
	/* Calculate the eye vector in world coordinates */
	VertexOut.E = cameraPosition - (modelMatrix * vPosition).xyz;
	
	VertexOut.color = vColor;
	
	/* Something like Math.random() ;) But it actually looks good on the cat */
	VertexOut.texCoord = vec2(0.5 - vPosition.x, 0.5 - vPosition.z);
}