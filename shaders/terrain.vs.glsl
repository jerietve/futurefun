#version 410

struct LightProperties
{
	vec4 lightPosition;
	vec4 lightAmbient;
	vec4 lightDiffuse;
	vec4 lightSpecular;
};

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 normalMatrix;
uniform mat4 projectionMatrix;
uniform vec3 cameraPosition;
/* This block is taken by uniform block indices 0 through lightPropertiesBlocks.length - 1 */
layout (std140) uniform LightPropertiesBlock
{
	uniform LightProperties lightProperties;
} lightPropertiesBlocks[2];

in vec4 vPosition;
in vec4 color;
in vec2 texCoord;
in vec4 vNormal;

out VertexData
{
	vec2 texCoord;
	float height;
	vec3 N;
	vec3[2] L;
	vec3 E;
} VertexOut;

void main()
{
	mat4 modelViewM = viewMatrix * modelMatrix;
	gl_Position = projectionMatrix * modelMatrix * viewMatrix * vPosition;
	
	/* Calculate the normal vector in world coordinates */
	VertexOut.N = (modelMatrix * vNormal).xyz;
	
	/* Calculate the light vector in world coordinates */
	for(int i = 0; i < lightPropertiesBlocks.length(); i++)
	{
		if(lightPropertiesBlocks[i].lightProperties.lightPosition.w == 0.0)
		{
			VertexOut.L[i] = lightPropertiesBlocks[i].lightProperties.lightPosition.xyz;
		}
		else
		{
			VertexOut.L[i] = lightPropertiesBlocks[i].lightProperties.lightPosition.xyz - (modelMatrix * vPosition).xyz;
		}
	}
	
	/* Calculate the eye vector in world coordinates */
	VertexOut.E = cameraPosition - (modelMatrix * vPosition).xyz;
	
	VertexOut.height = (vPosition).y;
	VertexOut.texCoord = texCoord;
}