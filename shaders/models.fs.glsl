#version 410

uniform sampler2D texture1;
uniform int isBoundingBox;
uniform int textured;

in VertexData
{
	vec4 color;
	vec3 N;
	vec3[2] L;
	vec3 E;
	vec2 texCoord;
};

out vec4 fragColor;

void calculateLighting(vec3, vec3[2], vec3, out vec4, out vec4, out vec4);

void main()
{
	if(isBoundingBox == 1)
	{
		fragColor = color;
		return;
	}

	vec4 ambient, diffuse, specular;
	calculateLighting(N, L, E, ambient, diffuse, specular);
	if(textured == 1) fragColor = texture(texture1, texCoord) * (ambient + diffuse + specular);
	else fragColor = color * (ambient + diffuse + specular);
}

